import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  items: any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.items = [
      {
        name: "Hard drive disk",
        description: "This hard drive is very slow",
        image: "assets/imgs/"
      }
    ]
  }

  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(ListPage, {
      item: item
    });
  }
}
