"use strict";

class Http {
    static ajax(method, url, data) {
        return new Promise((resolve, reject) => {
            let http = new XMLHttpRequest();
            http.open(method, url, true);
            http.send(data);

            http.addEventListener("error", (errorEvent) => reject(Error(errorEvent.message)));

            http.addEventListener("load", (event) => {
                if (http.status === 200) {
                    let response = JSON.parse(http.responseText);
                    resolve(response);
                }
                else {
                    reject(Error(http.statusText));
                }
            });
        });
    }
}
