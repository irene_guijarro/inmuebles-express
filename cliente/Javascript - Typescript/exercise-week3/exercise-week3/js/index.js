"use strict";
let arrEventItem = [];
arrEventItem = EventItem.getEvents().then(events => {
    arrEventItem = events;
    showEvents(arrEventItem);
});

let orderPrice = document.getElementById("orderPrice");
orderPrice.addEventListener("click", function(event){
    arrEventItem.sort((a, b) => {
        return b.price - a.price;
    });
    showEvents(arrEventItem);
});

let orderDate = document.getElementById("orderDate");
orderDate.addEventListener("click", function(event){
    arrEventItem.sort((a, b) => {
        return b.date - a.date;
    });
    showEvents(arrEventItem);
});

let search = document.getElementById("searchEvent");
search.addEventListener("keyup", function(event){
    let arrSearchResult = [];
    arrSearchResult = arrEventItem.filter(e => {
        return e.name.toLowerCase().includes(search.value.toLowerCase());
    });
    showEvents(arrSearchResult);
});

function showEvents(events) {
    let container = document.getElementById("eventsContainer");
    while(container.firstChild) {
        container.removeChild(container.firstChild);
    }

    events.forEach(e => {
        let divCard = e.toHTML();
        let btnDelete = divCard.querySelector('.card-title .btn');
        console.log(btnDelete);
        btnDelete.addEventListener("click", d => {
            if (confirm("Do you really want to delete the event " + e.name + "?")) {
                e.delete().then(data => {
                    if (data) {
                        let index = events.indexOf(e);
                        if(index >= 0) events.splice(index, 1);
                        showEvents(events);
                    }
                }).catch(error => console.error(error));
            } 
        });
        organizeCard(divCard);
    });
}

function organizeCard(divCard) {
    let eventsContainer = document.getElementById("eventsContainer");
    
    let cardDeck = document.querySelector(".card-deck:last-of-type");

    if (cardDeck === null || cardDeck.children.length === 2) {
        let newCardDeck = document.createElement("div");
        newCardDeck.className = "card-deck";
        newCardDeck.appendChild(divCard);
        eventsContainer.appendChild(newCardDeck);
    }
    else {
        cardDeck.appendChild(divCard);
    }
}