'use strict';
let btnSubmit = document.querySelector(".btn-primary");
window.addEventListener("load", function(event) {
    btnSubmit.addEventListener('click', function(e){
        e.preventDefault();
        validateForm();
    });
    let image = document.getElementById('image');
    image.addEventListener('change', function(event) {
        loadImage(event);
    })
})


function validateForm() {
    let name = document.getElementById("name");
    let description = document.getElementById("description");
    let date = document.getElementById("date");
    let price = document.getElementById("price");
    let image = document.getElementById("image");

    validateName(name);
    validateDescription(description);
    validateDate(date);
    validatePrice(price);
    validateImage(image);

    if (isEverythingValid()) {
        sendEventToServer(name, description, date, price, image);
    }
}

function validateName(name) {
    let strName = name.value;
    let regName = /^[a-z][a-z\s]*$/i;

    if (strName === "" || !regName.test(strName)) {
        isInvalid(name);
    }
    else {
        isValid(name);
    }
}

function isInvalid(field) {
    field.classList.remove("is-valid");
    field.classList.add("is-invalid");
}

function isValid(field) {
    field.classList.remove("is-invalid");
    field.classList.add("is-valid");
}

function validateDescription(description) {
    let strDescription = description.value;
    let regDesc = /[^$|\s]+/;

    if (regDesc.test(strDescription)) {
        isValid(description);
    }
    else {
        isInvalid(description);
    }
}

function validateDate(date) {
    let strDate = date.value;
    let regDate = /^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$/;

    if (regDate.test(strDate)) {
        isValid(date);
    }
    else {
        isInvalid(date);
    }
}

function validateImage(image) {
    if (image.value !== "") {
        isValid(image);
    }
    else {
        isInvalid(image);
    }
}

function loadImage(event) {
    let file = event.target.files[0];
    let reader = new FileReader();

    if (file) reader.readAsDataURL(file);

    reader.addEventListener('load', e => {
        document.getElementById("imgPreview").src = reader.result;
    });
}

function validatePrice(price) {
    let strPrice = price.value;
    let regPrice = /^(?=.*\d)\d*(?:\.\d\d)?$/;

    if (regPrice.test(strPrice)) {
        isValid(price);
    }
    else {
        isInvalid(price);
    }
}

function isEverythingValid() {
    let arrFormElements = document.querySelectorAll(".form-control");
    let somethingWrong = false;

    arrFormElements.forEach(i => {
        if (i.classList.contains("is-invalid")) {
            somethingWrong = true;
        }      
    });

    if (somethingWrong)
        return false;
    else
        return true;
}

function sendEventToServer(name, description, date, price, image) {
    let imgPreview = document.getElementById("imgPreview");
    let eventJSON = {
        "id" : "1",
        "name": name.value,
        "description": description.value,
        "date": date.value,
        "price": price.value,
        "image": imgPreview.src
    };

    let objEvent = new EventItem(eventJSON);

    objEvent.post().then(response => {
        if (response)
            location.assign("index.html");
        else
            alert(response.error);
    });
}