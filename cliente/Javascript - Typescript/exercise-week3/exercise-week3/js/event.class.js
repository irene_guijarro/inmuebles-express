"use strict";

class EventItem {
    constructor(eventJSON){
        this.id = eventJSON.id;
        this.name = eventJSON.name;
        this.date = new Date(eventJSON.date);
        this.description = eventJSON.description;
        this.price = new Number(eventJSON.price);
        this.image = eventJSON.image;
    }

    static getEvents() {
        return Http.ajax('GET', `http://${SERVER}/exercise3/events/`).then(response => {
            return response.events.map(event => new EventItem(event));
        });   
    }

    post() {
        this.date = `${this.date.getFullYear()}-${this.date.getMonth()}-${this.date.getDay()} ${this.date.getHours()}:${this.date.getMinutes()}:${this.date.getSeconds()}`;
        return Http.ajax('POST', `http://${SERVER}/exercise3/events/`, JSON.stringify(this))
            .then(response => {
                if (!response.ok) 
                    throw Error(response.error);
                else
                    return new EventItem(response.event);
        });
    }

    delete() {
        return Http.ajax('DELETE', `http://${SERVER}/exercise3/events/${this.id}`).then(response => {
            if (response.ok)
                return true;
            else
                throw Error(response.error);
        });
    }

    toHTML() {
        let divCard = document.createElement("div");
        divCard.className = "card";
    
        let img = document.createElement("img");
        img.className = "card-img-top"
        img.src = `http://${SERVER}/exercise3/img/${this.image}`;
    
        divCard.appendChild(img);
    
        let divCardBody = document.createElement("div");
        divCardBody.className = "card-body";
        divCard.appendChild(divCardBody);
    
        let h4 = document.createElement("h4");
        h4.className = "card-title";
        h4.innerText = this.name;
        divCardBody.appendChild(h4);

        let btn = document.createElement("button");
        btn.classList.add("btn");
        btn.classList.add("btn-danger");
        btn.classList.add("float-right");
        btn.innerText = "Delete";
        h4.appendChild(btn);
    
        let p = document.createElement("p");
        p.className = "card-text";
        p.innerText = this.description;
        divCardBody.appendChild(p);
    
        let divCardFooter = document.createElement("div");
        divCard.appendChild(divCardFooter);
    
        let small = document.createElement("small");
        small.className = "text-muted";
        small.innerText = this.date.toLocaleDateString();
        divCardFooter.appendChild(small);
    
        let span = document.createElement("span");
        span.className = "float-right";
        span.innerText = this.price + "€";
        small.appendChild(span);

        return divCard;
    }
}