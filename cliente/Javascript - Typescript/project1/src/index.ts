import { Http } from './classes/http';
import { EventItem } from './classes/event';
import { Auth } from './classes/authentication';
import { SERVER } from './contants';

let eventList: EventItem[] = [];

window.addEventListener('load', e => {
    let logout: HTMLAnchorElement = <HTMLAnchorElement>document.getElementById('logout');
    logout.addEventListener('click', e => {
        Auth.logout();
    });

    EventItem.getEvents().then(events => {
        eventList = events;
        showEvents(eventList);
    }); 

    document.getElementById("orderDate").addEventListener('click', e => {
        eventList.sort((e1, e2) => {
            e1.date = new Date(e1.date);
            e2.date = new Date(e2.date);
            return e1.date.getTime() - e2.date.getTime();
        });
        showEvents(eventList);
    });  

    document.getElementById("orderDistance").addEventListener('click', e => {
        eventList.sort((e1, e2) => e1.distance - e2.distance );
        document.getElementById("orderPrice").classList.remove("active");     
        showEvents(eventList);
    });

    document.getElementById("orderPrice").addEventListener('click', e => {
        console.log("hola");
        eventList.sort((e1, e2) => e1.price - e2.price );
        showEvents(eventList);
    });

    document.getElementById("searchEvent").addEventListener('keyup', e => {
        let searchHtml: HTMLInputElement = <HTMLInputElement>document.getElementById('searchEvent');
        let events = eventList.filter(ev => ev.title.toLocaleLowerCase().includes(searchHtml.value.toLocaleLowerCase()));
        showEvents(events);
    });
});

let showEvents = (events: EventItem[]) => {
    let container = document.getElementById("eventsContainer");
    while (container.firstChild) { // Delete all children
        container.removeChild(container.firstChild);
    }

    let deck;
    events.forEach((event, i) => {
        if(i % 2 === 0) {
            deck = document.createElement("div");
            deck.classList.add("card-deck");
            deck.classList.add("mb-4");
            document.getElementById("eventsContainer").appendChild(deck);
        }
        
        let eventCard = event.toHTML();
        eventCard.querySelector('.btn').addEventListener("click", e => {
            let del = confirm("Are you sure you want delete this event?");
            if(del) {
                event.delete().then(() => {
                    if(events !== eventList) events.splice(events.indexOf(event), 1);
                    eventList.splice(eventList.indexOf(event), 1);
                    showEvents(eventList);
                });
            }  
        });
        let imgTopCard: HTMLImageElement = <HTMLImageElement>eventCard.querySelector('.card-img-top');
        let cardTitle: HTMLHeadingElement = <HTMLHeadingElement>eventCard.querySelector('.card-title');

        imgTopCard.addEventListener("click", e => {
            location.assign('event-details.html');
        });

        cardTitle.addEventListener("click", e => {
            location.assign('event-details.html');
        });

        deck.appendChild(eventCard);
    });
}