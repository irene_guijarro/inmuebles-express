import { Geolocation } from './classes/geolocation';
import { GMaps } from './classes/gmaps';
import { EventItem } from './classes/event';
import { SERVER } from './contants';
import { Http } from './classes/http';

let gmap = null;

window.addEventListener('load', e => {
    let id: number = parseInt(location.search.substring(4));

    EventItem.getEvent(id).then(eventItem => {
        let eventContainer: HTMLDivElement = <HTMLDivElement>document.getElementById("cardContainer");
        
        eventContainer.removeChild(eventContainer.querySelector('.card'));
        eventContainer.appendChild(eventItem.toHTML());

        let address: HTMLDivElement = <HTMLDivElement>document.getElementById('address');
        address.innerText = eventItem.address;
        let latitude: number = eventItem.lat;
        let longitude: number = eventItem.lng;

        let coords: {latitude: number, longitude: number} = {latitude, longitude};

        gmap = new GMaps(coords, <HTMLDivElement>document.getElementById("map"));
        gmap.getMap().then(map => {            
            gmap.createMarker(latitude, longitude, "red");
        });

        document.querySelector('.btn-danger').addEventListener("click", e => {
            let del = confirm("Are you sure you want delete this event?");
            if(del) {
                eventItem.delete().then(() => {
                    location.assign('index.html');
                });
            }  
        });
    })
});