import { GMaps } from './classes/gmaps';
import { Geolocation } from './classes/geolocation';
import { EventItem } from './classes/event';
import {load, urlSettings} from 'google-maps-promise';

let gmap = null;
let lat: number;
let lng: number;
let address: string;

window.addEventListener('load', e => {
    let newEventForm: HTMLFormElement = <HTMLFormElement>document.getElementById('newEvent');
    newEventForm.image.addEventListener('change', loadImage);

    let btn: HTMLButtonElement = <HTMLButtonElement>document.querySelector(".btn");
    btn.addEventListener("click", e => {
        e.preventDefault();
        addEvent(); 
    });  
    
    Geolocation.getLocation().then(position => {
        gmap = new GMaps(position.coords, <HTMLDivElement>document.getElementById("map"));
        gmap.getMap().then(map => {            
            let autocomplete = gmap.getAutocomplete(document.getElementById("address"));
            changeAutocomplete(autocomplete); // Initializes place_changed event on the autocomplete 
        });

    }).catch(error => {
        console.error(error);
    });
    
});

function changeAutocomplete(autocomplete) {
    let map = gmap.map; // The map should be already loaded

    google.maps.event.addListener(autocomplete, 'place_changed', event => {
        let place = autocomplete.getPlace();
        if (!place.geometry) return;
        
        map.panTo(place.geometry.location);
        gmap.createMarker(place.geometry.location.lat(), place.geometry.location.lng(), "blue");
        address = place.formatted_address;
        lat = place.geometry.location.lat();
        lng = place.geometry.location.lng();
    }); 
}

function addEvent() {
        let titleHTML: HTMLInputElement = <HTMLInputElement>document.getElementById('title');
        let title: string = titleHTML.value;
        let dateHTML: HTMLInputElement =  <HTMLInputElement>document.getElementById('date');
        let date: string = dateHTML.value;
        let descriptionHTML: HTMLInputElement =  <HTMLInputElement>document.getElementById('description');
        let description: string = descriptionHTML.value;
        let priceHTML: HTMLInputElement =  <HTMLInputElement>document.getElementById('price');
        let price: number = parseInt(priceHTML.value);

        let imgHTML: HTMLImageElement = <HTMLImageElement>document.getElementById("imgPreview");
        let image: string = imgHTML.src;

        let newEvent: {title: string, date: string, description: string, price: number,
            address: string, image: string, lat: number, lng: number} = {title, date, description, 
            price, address, image, lat, lng};
        let event: EventItem = new EventItem(newEvent);

        event.post().then(event => {
            location.assign('index.html');
        }).catch(error => {
            let errorInfo: HTMLParagraphElement = <HTMLParagraphElement>document.getElementById('errorInfo');
            errorInfo.innerText = error;
        });
}

function loadImage(event) {
    let file = event.target.files[0];
    let reader = new FileReader();

    if (file) reader.readAsDataURL(file);

    reader.addEventListener('load', e => {
        let imgHTML: HTMLImageElement = <HTMLImageElement>document.getElementById("imgPreview");
        let image: string = imgHTML.src = reader.result; 
    });
}