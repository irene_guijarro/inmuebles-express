import {load as loadGMaps, urlSettings as gmapsSettings } from 'google-maps-promise';

export class GMaps {
    coords: {latitude: number, longitude: number};
    divMap: HTMLDivElement;
    map: google.maps.Map;
    autocomplete: google.maps.places.Autocomplete;

    constructor(coords: { latitude: number, longitude: number }, divMap: HTMLDivElement) { 
        gmapsSettings.key = 'AIzaSyDsm-KdVSm6e36pNcmjOz9CbxB9Yq_9fCc';
        gmapsSettings.language = 'es';
        gmapsSettings.region = 'ES';
        gmapsSettings.libraries = ['geometry', 'places'];
        this.map = null;
        this.autocomplete = null;
        this.coords = coords;
        this.divMap = divMap;
    }
    getMap(): Promise<google.maps.Map> { // Returns the map
        if (this.map !== null) return new Promise((resolve, reject) => resolve(this.map));

        return loadGMaps().then((gmaps) => {
            let gLatLng = new google.maps.LatLng(this.coords.latitude, this.coords.longitude);

            let options = {
                zoom: 17,
                center: gLatLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            this.map = new google.maps.Map(this.divMap, options);
            return this.map;
        });
    } 
    // Returns the marker created
    createMarker(lat: number, lng: number, color: string): google.maps.Marker { 
        if (this.map === null) return;

        let gLatLng = new google.maps.LatLng(lat, lng);
        var opts = {
            position: gLatLng,
            map: this.map,
            icon: 'http://maps.google.com/mapfiles/ms/icons/' + color + '-dot.png'
        };

        return new google.maps.Marker(opts);
    }
    // Creates the Google Places Autocomplete component and returns it (don't put the change event here!)
    getAutocomplete(input: HTMLInputElement) {
        if (this.autocomplete !== null) return this.autocomplete;

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', this.map);
        this.autocomplete = autocomplete;
        return autocomplete;
    }   
}
