import { IResponse } from '../interfaces/iresponse';
import { SERVER } from '../contants';
import { Http } from './http';
import {IUser} from '../interfaces/iuser';

export class Auth {
    // Check the login service and save the token here
    static login(loginInfo: { email: string, password: string }): Promise<IResponse> { 
        return Http.ajax("POST", "http://" + SERVER + "/auth/login", loginInfo).then(response => {
            if (!response.ok)
                throw Error(response.error);
            else {
                localStorage.setItem("token", response.token);
                return response;
            }
        });
    }
    //Use the register service and return true or throw the errors
    static register(userInfo: IUser): Promise<IResponse> {
        return Http.ajax("POST", "http://" +  SERVER + "/auth/register", userInfo).then(response => {
            return response;   
        });
    }
    //Use the token service to check if the token is valid (return true)
    static checkToken(): Promise<boolean> { 
        return Http.ajax("GET", "http://" + SERVER + "/auth/token").then(response => {
            if (response.ok) {
                return true;
            }
            else
                return false;
        });
    }
    // Remove the token
    static logout() {
        localStorage.removeItem("token");
        location.assign('login.html');
    }
}