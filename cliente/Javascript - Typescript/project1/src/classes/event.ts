import { IEvent } from '../interfaces/ievent';
import { Http } from './http';
import { SERVER } from '../contants';
import { IUser } from '../interfaces/iuser';
import { IMG_EVENTS_PATH } from '../contants';
import { IMG_PROFILE_PATH } from '../contants';


export class EventItem implements IEvent {
    id?: number;
    title: string;
    date: Date;
    description: string;
    image: string;
    price: number;
    address: string;
    lat: number;
    lng: number;
    creator?: IUser;
    mine?: boolean;
    attend?: boolean;
    distance?: number;

    constructor(eventJSON: IEvent) { 
        this.id = eventJSON.id;
        this.title = eventJSON.title;
        this.date = new Date(eventJSON.date);
        this.description = eventJSON.description;
        this.image = `http://${IMG_EVENTS_PATH}${eventJSON.image}`;
        this.price = eventJSON.price;
        this.address = eventJSON.address;
        this.lat = eventJSON.lat;
        this.lng = eventJSON.lng;
        this.creator = eventJSON.creator;
        this.mine = eventJSON.mine;
        this.attend = eventJSON.attend;
        this.distance = eventJSON.distance;
    }
    static getEvents(): Promise<EventItem[]> { 
        return Http.ajax("GET", "http://" + SERVER + "/events").then((response) => 
        response.events.map(e => new EventItem(e)));
    }
    static getEvent(id: number): Promise<EventItem> { 
        return Http.ajax("GET", "http://" + SERVER + "/events/" + id).then((response) => {
            return new EventItem(response.event);
        });
    }

    post(): Promise<Boolean> { 
        return Http.ajax('POST', `http://${SERVER}events`, this)
            .then((response) => {
                if (response.ok) {
                    return true;
                } else
                    throw response.error;
            });
    }
    delete(): Promise<boolean> { 
        return Http.ajax('DELETE', `http://${SERVER}events/${this.id}`, this)
        .then((response) => {
            if(response.ok)
                return true;
            else
                throw response.error;  
        });
    }
    // If attend == true call the service to delete this property and viceversa (update the property in the object)
    /*toggleAttend(): Promise<boolean> { }
    getAttendees(): Promise<IUser[]> {
        return Http.ajax("GET", `${SERVER}/events/attend/${this.id}`)
            .then((response: IResponse) => response.users.map(u => new User(u)));
    }*/

    // You'll need to send more info to the Handlebars template than in exercise 4
    toHTML(): HTMLDivElement { 
        let card = document.createElement("div");
        card.classList.add("card");
        let eventTemplate = require('../../templates/event.handlebars');

        card.innerHTML = eventTemplate({
            id: this.id,
            title: this.title,
            date: `${this.date.getDate()}/${this.date.getMonth()}/${this.date.getFullYear()}`,
            description: this.description,
            image: this.image,
            price: this.price.toFixed(2),
            address: this.address,
            lat: this.lat,
            lng: this.lng,
            creator: this.creator,
            creatorAvatar: `http://${IMG_PROFILE_PATH}${this.creator.avatar}`,
            mine: this.mine,
            attend: this.attend,
            distance: this.distance.toFixed(2)
        });

        return card;
    }
}
