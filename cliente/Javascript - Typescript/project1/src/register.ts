import { Auth } from './classes/authentication';
import { Geolocation } from './classes/geolocation';

let avatar: string;

window.addEventListener("load", function (event) {
    Geolocation.getLocation().then(position => {
        let latHTML: HTMLInputElement = <HTMLInputElement>document.getElementById("lat");
        latHTML.placeholder = position.coords.latitude.toString();
        let lngHTML: HTMLInputElement = <HTMLInputElement>document.getElementById("lng");
        lngHTML.placeholder = position.coords.longitude.toString();
    });

    let newEventForm: HTMLFormElement = <HTMLFormElement>document.getElementById('form-register');
    newEventForm.image.addEventListener('change', loadImage);

    register();
});

let register = () => {
    document.getElementById("form-register").addEventListener("submit", e => {
        e.preventDefault();
    
        let nameHTML: HTMLInputElement = <HTMLInputElement>document.getElementById("name");
        let name: string = nameHTML.value;
        let emailHTML: HTMLInputElement = <HTMLInputElement>document.getElementById("email");
        let email: string = emailHTML.value;
        let emailHTML2: HTMLInputElement = <HTMLInputElement>document.getElementById("email");
        let email2: string = emailHTML2.value;
        let passwordHTML: HTMLInputElement = <HTMLInputElement>document.getElementById("password");
        let password: string = passwordHTML.value;
        let lat: number;
        let lng: number;
    
        Geolocation.getLocation().then(position => {
            lat = position.coords.latitude;
            lng = position.coords.longitude;
        });
    
        let userInfo: {name: string, avatar: string, email: string, email2: string, password: string, lat: number, lng: number} =
            {name, avatar, email, email2, password, lat, lng};
    
        Auth.register(userInfo).then(response => {
            if (response.ok)
                location.assign('login.html');
        }).catch(error => {
            let errorInfo: HTMLParagraphElement = <HTMLParagraphElement>document.getElementById('errorInfo');
            errorInfo.innerText = error;
        });
    });
}

function loadImage(event) {
    let file = event.target.files[0];
    let reader = new FileReader();

    if (file) reader.readAsDataURL(file);

    reader.addEventListener('load', e => {
        let imgHTML: HTMLImageElement = <HTMLImageElement>document.getElementById("imgPreview");
        avatar = imgHTML.src = reader.result; 
    });
}
