import { SERVER } from './contants';
import { Http } from './classes/http';
import { Auth } from './classes/authentication';

window.addEventListener('load', e => {
    e.preventDefault();
    Auth.checkToken().then(isOk => {
        if (isOk)
            location.assign('index.html');
    });

    let form: HTMLFormElement = <HTMLFormElement> document.getElementById("form-login");
    form.addEventListener('submit', e => {
        e.preventDefault();
    
        let emailHTML: HTMLInputElement = <HTMLInputElement>document.getElementById("email");
        let email: string = emailHTML.value;
        let passwordHTML: HTMLInputElement = <HTMLInputElement>document.getElementById("password");
        let password: string = passwordHTML.value;
        let loginInfo: {email: string, password: string } = {email, password};

        Auth.login(loginInfo).then(response => {
            if (response.ok)
                location.assign('index.html');
        }).catch(error => {
            let errorInfo: HTMLParagraphElement = <HTMLParagraphElement>document.getElementById('errorInfo');
            errorInfo.innerText = error;
        });
    });
});
