webpackJsonp([4],{

/***/ 26:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__classes_authentication__ = __webpack_require__(2);

window.addEventListener('load', e => {
    e.preventDefault();
    __WEBPACK_IMPORTED_MODULE_0__classes_authentication__["a" /* Auth */].checkToken().then(isOk => {
        if (isOk)
            location.assign('index.html');
    });
    let form = document.getElementById("form-login");
    form.addEventListener('submit', e => {
        e.preventDefault();
        let emailHTML = document.getElementById("email");
        let email = emailHTML.value;
        let passwordHTML = document.getElementById("password");
        let password = passwordHTML.value;
        let loginInfo = { email, password };
        __WEBPACK_IMPORTED_MODULE_0__classes_authentication__["a" /* Auth */].login(loginInfo).then(response => {
            if (response.ok)
                location.assign('index.html');
            else {
                let errorInfo = document.getElementById('errorInfo');
                errorInfo.innerText = response.error;
            }
        });
    });
});


/***/ })

},[26]);
//# sourceMappingURL=login.bundle.js.map