webpackJsonp([1],{

/***/ 27:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__classes_authentication__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__classes_geolocation__ = __webpack_require__(28);


window.addEventListener("load", function (event) {
    __WEBPACK_IMPORTED_MODULE_1__classes_geolocation__["a" /* Geolocation */].getLocation().then(position => {
        let latHTML = document.getElementById("lat");
        latHTML.placeholder = position.coords.latitude.toString();
        let lngHTML = document.getElementById("lng");
        lngHTML.placeholder = position.coords.longitude.toString();
    });
    register();
});
let register = () => {
    document.getElementById("form-register").addEventListener("submit", e => {
        e.preventDefault();
        let nameHTML = document.getElementById("name");
        let name = nameHTML.value;
        let emailHTML = document.getElementById("email");
        let email = emailHTML.value;
        let emailHTML2 = document.getElementById("email");
        let email2 = emailHTML2.value;
        let passwordHTML = document.getElementById("password");
        let password = passwordHTML.value;
        let avatar = "";
        let lat;
        let lng;
        __WEBPACK_IMPORTED_MODULE_1__classes_geolocation__["a" /* Geolocation */].getLocation().then(position => {
            lat = position.coords.latitude;
            lng = position.coords.longitude;
        });
        let userInfo = { name, avatar, email, email2, password, lat, lng };
        __WEBPACK_IMPORTED_MODULE_0__classes_authentication__["a" /* Auth */].register(userInfo).then(response => {
            if (response.ok)
                location.assign('login.html');
            else {
                let errorInfo = document.getElementById('errorInfo');
                errorInfo.innerText = response.errors[0];
            }
        });
    });
};


/***/ }),

/***/ 28:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class Geolocation {
    // Return the position of the Geolocation service or throw an error message
    static getLocation() {
        return new Promise((resolve, reject) => {
            navigator.geolocation.getCurrentPosition(position => {
                resolve(position);
            }, error => {
                switch (error.code) {
                    case error.PERMISSION_DENIED:// User didn't allow the web page to retrieve location
                        reject("User denied the request for Geolocation.");
                        break;
                    case error.POSITION_UNAVAILABLE:// Couldn't get the location
                        reject("Location information is unavailable.");
                        break;
                    case error.TIMEOUT:// The maximum amount of time to get location information has passed
                        reject("The request to get user location timed out.");
                        break;
                    default:
                        reject("An unknown error occurred.");
                        break;
                }
            });
        });
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Geolocation;



/***/ })

},[27]);
//# sourceMappingURL=register.bundle.js.map