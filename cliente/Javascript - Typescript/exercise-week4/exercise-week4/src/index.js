"use strict";
import {EventItem, getEvents} from './event.class.js';
import {Http} from './http.class.js';

let eventList = [];
let eventShow = [];

window.addEventListener("load", e => {
    EventItem.getEvents().then(events => {
        eventList = events;
        eventShow = [...eventList]; // Copy array
        showEvents(eventShow);
    });

    document.getElementById("orderDate").addEventListener('click', e => {
        eventShow.sort((e1, e2) => e1.date.getTime() - e2.date.getTime() );
        document.getElementById("orderPrice").classList.remove("active");        
        e.target.classList.add("active");
        showEvents(eventShow);
    });

    document.getElementById("orderPrice").addEventListener('click', e => {
        eventShow.sort((e1, e2) => e1.price - e2.price );
        document.getElementById("orderDate").classList.remove("active");
        e.target.classList.add("active");
        showEvents(eventShow);
    });

    document.getElementById("searchEvent").addEventListener('keyup', e => {
        eventShow = eventList.filter(ev => ev.name.toLocaleLowerCase().includes(e.target.value.toLocaleLowerCase()));
        document.getElementById("orderPrice").classList.remove("active");   
        document.getElementById("orderDate").classList.remove("active");
        showEvents(eventShow);
    });
});

function showEvents(events) {
    let container = document.getElementById("eventsContainer");
    while (container.firstChild) { // Delete all children
        container.removeChild(container.firstChild);
    }

    let deck;
    events.forEach((event, i) => {
        if(i % 2 === 0) {
            deck = document.createElement("div");
            deck.classList.add("card-deck");
            deck.classList.add("mb-4");
            document.getElementById("eventsContainer").appendChild(deck);
        }
        
        let eventCard = event.toHTML();
        eventCard.querySelector('.card-title .btn').addEventListener("click", e => {
            let del = confirm("Are you sure you want delete this event?");
            if(del) {
                event.delete().then(() => {
                    eventShow.splice(eventShow.indexOf(event), 1);
                    eventList.splice(eventList.indexOf(event), 1);
                    showEvents(eventShow);
                });
            }  
        });
        deck.appendChild(eventCard);
    });
}