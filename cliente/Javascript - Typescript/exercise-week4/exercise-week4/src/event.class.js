"use strict";
import eventTemplate from '../templates/event.handlebars';
import {Http} from './http.class.js';
import {SERVER, IMG_URL} from './constants.js';


export class EventItem {
    constructor({id = -1, name, date, description, image, price}) {
        this.id          = id;
        this.name        = name;
        this.date        = new Date(date);
        this.description = description;
        this.image       = `${IMG_URL}/${image}`;
        this.price       = +price;
    }

    static getEvents() { // Returns Promise<Array<EventItem>> with the array of EventItems
        return Http.ajax('GET', `${SERVER}/events`)
            .then((response) => response.events.map(e => new EventItem(e)));
    }

    post() { // Returns Promise<true> or Promise<EventItem> 
        this.date = `${this.date.getFullYear()}-${this.date.getMonth()}-${this.date.getDay()}`;
        return Http.ajax('POST', `${SERVER}/events`, this)
            .then((response) => {
                if(response.ok) {
                    this.id = response.event.id;
                    this.image = `${IMG_URL}/${image}`;
                    return new EventItem(response.event);
                } else
                    throw response.error;  
            });
    }

    delete() { // Returns Promise<true>
        return Http.ajax('DELETE', `${SERVER}/events/${this.id}`, this)
        .then((response) => {
            if(response.ok)
                return true;
            else
                throw response.error;  
        });
    }

    toHTML() {
        let eventJSON = {
            "id": this.id,
            "name": this.name,
            "date": this.date.toLocaleString(),
            "description": this.description,
            "image": this.image,
            "price": this.price.toFixed(2)
        };  

        let htmlCard = eventTemplate(eventJSON);
        var card = document.createElement('div')
        card.innerHTML = htmlCard;
        return card;
    }
    
}
