'use strict';
import {EventItem} from './event.class.js';

let newEventForm = null;

function testInputExpr(input, expr) {
    input.classList.remove("is-valid", "is-invalid");
    if (expr.test(input.value)) {
        input.classList.add("is-valid");
        return true;
    } else {
        input.classList.add("is-invalid");
        return false;
    }
}

function validatePrice() {
    return testInputExpr(document.getElementById("price"),
        /[0-9]+(\.[0-9]{1,2})?/);
}

function validateName() {
    return testInputExpr(document.getElementById("name"),
        /[a-z][a-z ]*/);
}

function validateDescription() {
    return testInputExpr(document.getElementById("description"),
        /.*\S.*/);
}

function validateDate() {
    return testInputExpr(document.getElementById("date"), 
                         /\d{4}-\d{2}-\d{2}/);
}

function validateImage() {
    let imgInput = document.getElementById("image");

    imgInput.classList.remove("is-valid", "is-invalid");
    if(imgInput.files.length > 0) {
        imgInput.classList.add("is-valid");
        return true;
    } else {
        imgInput.classList.add("is-invalid");
        return false;
    }
}

function validateForm(event) {
    event.preventDefault();
    let name = newEventForm.name.value;
    let image = document.getElementById("imgPreview").src;
    let date = newEventForm.date.value;
    let desc = newEventForm.description.value;
    let price = newEventForm.price.value;

    if (validateName() && validateDate() && validateDescription() &&
        validatePrice() && validateImage()) {
        let newEvent = new EventItem({name, image, date, description: desc, price});
        newEvent.post().then((event => {
            location.assign("index.html");
        })).catch(error => {
            alert(error);
        });
    }
}

function loadImage(event) {
    let file = event.target.files[0];
    let reader = new FileReader();

    if (file) reader.readAsDataURL(file);

    reader.addEventListener('load', e => {
        document.getElementById("imgPreview").src = reader.result;
    });
}

window.addEventListener("load", e => {
    newEventForm = document.getElementById("newEvent");

    newEventForm.image.addEventListener('change', loadImage);

    newEventForm.addEventListener('submit', validateForm);
});
