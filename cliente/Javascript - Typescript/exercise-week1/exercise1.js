//Irene Guijarro Martínez

/**
 * Part 1
 * Create a function that receives 2 strings. It will print the longest of them (length). 
 * If a parameter is not a string, print an error instead. Call it several times.
 */
console.log("EXERCISE 1 - PART 1");

function exercise1(str1, str2) {
    if ((typeof str1 === "string") && (typeof str2 === "string")) {
        if (str1.length > str2.length) {
            console.log(str1);
        }
        else {
            console.log(str2);
        }
    }
    else {
        console.log("Error, one of them is not a string!");
    }
}

exercise1("Irene", "Hello");
exercise1("AAA", "BBBBB");
exercise1(3, "C");

/**
 * Part 2
 * Create an array with different value types (number, boolean, string, ...).
 * From that array create another that has values from the first converted to numbers
 * but not the ones that couldn't be converted (NaN). Don't use any for, foreach, while loop.
 * Instead use array methods like map and filter. Print the resulting array 
 */
console.log("EXERCISE 1 - PART 2");

let array1 = [1, true, "5", 4.2, "Hello"];
let array2 = array1.map(n => Number(n));
array2 = array2.filter(n => isNaN(n) === false);
console.log(array2);

/**
 * Part 3
 * Create a function that receives 3 parameters with default values (product -> "Generic product",
 * price -> 100, tax 21). Convert the product's name into string and the other 2 parameters into numbers.
 * If price or tax cannot be converted to number, show an error.
 * Call this function several times, omitting parameters or sending not numeric values.
 */
console.log("EXERCISE 1 - PART 3");

function part3(product, price, tax) {
    product = String(product);
    price = Number(price);
    tax = Number(tax);

    if (!isFinite(tax) || !isFinite(price)) {
        console.log("Error, tax or price not numbers");
    }
    else {
        console.log("Everything OK!");
    }
}

part3("Generic product", 100, 21);
part3("Mouse", "jjj", 10);
part3("Water", 1.25, "oo");


/**
 * Part 4
 * Create an array with 4 values and do the following (use the correct array methods).
 * Add 2 elements at te beginnning an 2 more at the end.
 * Delete positions from 3 to 5 (included)
 * Insert 2 elements more before the last element.
 * Show the resulting array with elements separated by '=>' (don't use any loop).
 */
console.log("EXERCISE 1 - PART 4");

let animals = ["Cat", "Dog", "Snake", "Fish"];
animals.unshift("Koala", "Headhog");
animals.push("Seal", "Tiger");
console.log(animals);

animals.splice(3,3);
console.log(animals);

animals.splice((animals.length) -1, 0,"Goldfish", "Zebra");
console.log(animals);

console.log(animals.join(" => "));


/**
 * Part 5
 * Create an array with several strings. Using the reduce method, return a string
 * that is a concatenation of the first letter of every string in the array.
 */
console.log("EXERCISE 1 - PART 5");

let names = ["Irene", "Celia", "Juan", "Loli"];
console.log((names.reduce((finalString, name) => finalString + name.charAt(0), "")));


/**
 * Part 6
 * Create a function that receives an array as a parameter and destructure the first three elements
 * in the function declaration's parameters. They will have default values of 1, 2 and 3 if any of those
 * positions is empty. Show what you've received in the function's body.
 * Call that function several times with numeric arrays (can be empty also)
 */
console.log("EXERCISE 1 - PART 6");

function destructure(array) {
    let [first, second, third] = array;
    if (first === undefined) {
        first = 1;
    }
    if (second === undefined) {
        second = 2;
    }
    if (third === undefined) {
        third = 3;
    }

    return `Numbers are: ${first}, ${second}, ${third}`;
}

console.log(destructure([166, 4, 0, 4, 5, 6]));
console.log(destructure([, , , 4, 5, 6]));

/**
 * Part 7
 * Create a funcion that can receive as many numbers as you wan't by parameter. Use rest to group them in
 * an array and print the ones that are pair. Call this function several times with different arguments.
 */
console.log("EXERCISE 1 - PART 7");

function group(...num) {
    return num.filter(n => n % 2 === 0);
}

console.log(group(2, 4, 5, 7, 9));

/**
 * Part 8
 * Create a Map object containing several key-value pairs. The key will be a person's name and the value will be 
 * the subjects this person is studying this year. Iterate through this map and show on every iteration the
 * person's name and his/her subjects separated by '-' (Example -> Peter is studying: Math-History-English).
 */
console.log("EXERCISE 1 - PART 8");

let person1 = {name: "Irene"};
let person2 = {name: "Marcus"};

let subjects = new Map();
subjects.set(person1, ["History", "Sciencie", "PE"]);
subjects.set(person2, ["Biology", "Maths"]);

subjects.forEach((subArray, person) => console.log(person.name + " is studying: " + subArray.join("-")));