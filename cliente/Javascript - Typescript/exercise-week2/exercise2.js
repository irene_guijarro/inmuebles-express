//Irene Guijarro Martínez
"use strict";
let btnSubmit = document.querySelector(".btn-primary");

btnSubmit.addEventListener('click', function(e){
    e.preventDefault();
    validateForm();
});

function validateForm() {
    let name = document.getElementById("name");
    let description = document.getElementById("description");
    let date = document.getElementById("date");
    let price = document.getElementById("price");
    let image = document.getElementById("image");

    validateName(name);
    validateDescription(description);
    validateDate(date);
    validatePrice(price);
    validateImage(image);

    if (isEverythingValid()) {
        addNewEvent(name, description, date, price, image);
        resetForm();
    }
}

function validateName(name) {
    let strName = name.value;
    let regName = /^[a-z][a-z\s]*$/i;

    if (strName === "" || !regName.test(strName)) {
        isInvalid(name);
    }
    else {
        isValid(name);
    }
}

function isInvalid(field) {
    field.classList.remove("is-valid");
    field.classList.add("is-invalid");
}

function isValid(field) {
    field.classList.remove("is-invalid");
    field.classList.add("is-valid");
}

function validateDescription(description) {
    let strDescription = description.value;
    let regDesc = /[^$|\s]+/;

    if (regDesc.test(strDescription)) {
        isValid(description);
    }
    else {
        isInvalid(description);
    }
}

function validateDate(date) {
    let strDate = date.value;
    let regDate = /^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$/;

    if (regDate.test(strDate)) {
        isValid(date);
    }
    else {
        isInvalid(date);
    }
}

function validateImage(image) {
    if (image.value !== "") {
        isValid(image);
    }
    else {
        isInvalid(image);
    }
}

function validatePrice(price) {
    let strPrice = price.value;
    let regPrice = /^(?=.*\d)\d*(?:\.\d\d)?$/;

    if (regPrice.test(strPrice)) {
        isValid(price);
    }
    else {
        isInvalid(price);
    }
}

function isEverythingValid() {
    let arrFormElements = document.querySelectorAll(".form-control");
    let somethingWrong = false;

    arrFormElements.forEach(i => {
        if (i.classList.contains("is-invalid")) {
            somethingWrong = true;
        }      
    });

    if (somethingWrong)
        return false;
    else
        return true;
}

function addNewEvent(name, description, date, price, image) {
    let divCard = document.createElement("div");
    divCard.className = "card";

     let img = document.createElement("img");
    img.className = "card-img-top"
    img.src = document.getElementById("imgPreview").src;

    divCard.appendChild(img);

    let divCardBody = document.createElement("div");
    divCardBody.className = "card-body";
    divCard.appendChild(divCardBody);

    let h4 = document.createElement("h4");
    h4.className = "card-title";
    h4.innerText = name.value;
    divCardBody.appendChild(h4);

    let p = document.createElement("p");
    p.className = "card-text";
    p.innerText = description.value;
    divCardBody.appendChild(p);

    let divCardFooter = document.createElement("div");
    divCard.appendChild(divCardFooter);

    let small = document.createElement("small");
    small.className = "text-muted";
    let newDate = new Date(date.value);
    small.innerText = newDate.toLocaleDateString();
    divCardFooter.appendChild(small);

    let span = document.createElement("span");
    span.className = "float-right";
    span.innerText = price.value + "€";
    small.appendChild(span);

    organizeCard(divCard);
}

function organizeCard(divCard) {
    let eventsContainer = document.getElementById("eventsContainer");
    
    let cardDeck = document.querySelector(".card-deck:last-of-type");

    if (cardDeck === null || cardDeck.children.length === 2) {
        let newCardDeck = document.createElement("div");
        newCardDeck.className = "card-deck";
        newCardDeck.appendChild(divCard);
        eventsContainer.appendChild(newCardDeck);
    }
    else {
        cardDeck.appendChild(divCard);
    }
}

function resetForm() {
    let arrFormElements = document.querySelectorAll(".form-control");

    arrFormElements.forEach(i => {
        if (i.classList.contains("is-invalid")) {
            i.classList.remove("is-invalid");
        }
        else if (i.classList.contains("is-valid")) {
            i.classList.remove("is-valid");
        }
        i.value = "";

        if (i.id === "image") {
            document.getElementById("imgPreview").src = "";
        }
    });

}

document.getElementById('image').addEventListener('change', event => {
    let file = event.target.files[0];
    let reader = new FileReader();
    if (file) reader.readAsDataURL(file);
    reader.addEventListener('load', e => {
    document.getElementById("imgPreview").src = reader.result;
    });
});