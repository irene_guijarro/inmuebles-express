import { IEvent } from '../interfaces/i-event';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ae-events-show',
  templateUrl: './events-show.component.html',
  styleUrls: ['./events-show.component.css']
})
export class EventsShowComponent implements OnInit {
    events: IEvent[] = [];
    newEvent: IEvent = {
        title: '',
        image: '',
        date: '',
        description: '',
        price: 0
    };

    changeImage(fileInput: HTMLInputElement) {
        if (!fileInput.files || fileInput.files.length === 0) { return; }
        const reader: FileReader = new FileReader();
        reader.readAsDataURL(fileInput.files[0]);
        reader.addEventListener('loadend', e => {
            this.newEvent.image = reader.result;
        });
    }

    constructor() { }

    ngOnInit() {
    }

    addEvent() {
        this.events.push(this.newEvent);
        this.initializaNewEvent();
    }

    delete(i) {
        this.events.splice(i, 1);
    }

    private initializaNewEvent() {
        this.newEvent = {
            title: '',
            image: '',
            date: '',
            description: '',
            price: 0
        };
    }

}
