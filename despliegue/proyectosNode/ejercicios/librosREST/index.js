const http = require('http');
const modelo = require('./modelo');

let listarLibros = () => {
    return JSON.stringify(modelo.libros.listarLibros());
}

let modificarLibro = (libro) => {
    let resultado = {};
    if (libro.id && modelo.libros.modificarLibro(libro.id))
}

let borrarLibro = (libro) => {

}

let nuevoLibro = (libro) => {
    let resultado = {};
    if (libro.id && modelo.libros.nuevoLibro(libro.id,
        libro.titulo, libro.autor, libro.precio)) {
        resultado.error = false;
        resultado.mensajeError = "";
    } else {
        resultado.error = true;
        resultado.mensajeError = "Error al añadir el nuevo libro";
    }
    return JSON.stringify(resultado);
}


let fichaLibro = (id) => {
    if ((modelo.libros.buscarLibroPorId(id) === undefined)) {
        //TODO:
    }
    else
        return JSON.stringify(modelo.libros.buscarLibroPorId(id));
}

let atenderPeticion = (request, response) => {
    let responseCerrado = false;
    response.writeHead(200, { "Content-Type": "text/plain" });
    if (request.method === 'GET') {
        if (request.url === '/libros') {
            response.write(listarLibros());
        }
    } else if (request.method === 'POST' &&
        request.url === '/libro') {
        responseCerrado = true;
        let body = [];
        request.on('data', (chunk) => {
            body.push(chunk);
        }).on('end', () => {
            body = Buffer.concat(body).toString();
            response.write(nuevoLibro(JSON.parse(body)));
            response.end();
        });
    } else if (request.url.startsWith("/libro/")) {
        let partes = request.url.split('/');
        let id = partes[2];
        response.write(fichaLibro(id));
    } else if (request.method === 'POST' && request.url === '/libro') {

    } else if (request.method === 'DELETE') {
        let partes = request.url.split('/');
        response.write(borrarLibro(partes[2]));
    }
    if (!responseCerrado)   
        response.end();
}

http.createServer(atenderPeticion).listen(8080);