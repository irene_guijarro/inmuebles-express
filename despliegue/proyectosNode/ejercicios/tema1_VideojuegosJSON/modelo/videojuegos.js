const fs = require('fs');
const archivo = 'videojuegos.json';

let cargarVideojuegos = () => {
    try {
        return JSON.parse(fs.readFileSync(archivo, 'utf8'));
    } catch (e) {
        return [];
    }
};
let guardarVideojuegos = (videojuegos) => {
    fs.writeFileSync(archivo, JSON.stringify(videojuegos));
};

let buscarLibroPorId = (id) => {
    let videojuegos = cargarVideojuegos();
    let resultado = videojuegos.filter((videojuegos) => videojuegos.id == id);
    if (resultado.length > 0)
        return resultado[0];
};

let nuevoVideojuego = (id, titulo, fabricante, categoria, anyo_lanzamiento) => {
    if (!buscarVideojuegoPorId(id)) {
        let videojuegos = cargarVideojuegos();
        let nuevo = {
            id: id,
            titulo: titulo,
            fabricante: fabricante,
            categoria: categoria,
            anyo_lanzamiento: año_lanzamiento
        };
        videojuegos.push(nuevo);
        guardarVideojuegos(videojuegos);
    }
};

let borrarVideojuego = (id) => {
    let videojuegos = cargarVideojuegos();
    let videojuegosFiltrados = videojuegos.filter((videojuego) => videojuego.id != id);
    if (videojuegosFiltrados.length != videojuegos.length)
        guardarVideojuegos(videojuegosFiltrados);
    return videojuegosFiltrados.length != videojuegos.length;
}

let listarVideojuegosDesdeAnyo = (anyo) => {
    let videojuegos = cargarVideojuegos();
    let videojuegosFiltrados = videojuegos.filter((videojuego) => videojuego.anyo_lanzamiento >= anyo);
    return videojuegosFiltrados;
}

let listarVideojuegosPorCategoria = (categoria) => {
    let videojuegos = cargarVideojuegos();
    let videojuegosFiltrados = videojuegos.filter((videojuego) => videojuego.categoria == categoria);
    return videojuegosFiltrados;
}

module.exports = {
    listarVideojuego: cargarVideojuegos,
    nuevoVideojuego: nuevoVideojuego,
    borrarVideojuego: borrarVideojuego,
    listarVideojuegosDesdeAnyo: listarVideojuegosDesdeAnyo,
    listarVideojuegosPorCategoria: listarVideojuegosPorCategoria
};