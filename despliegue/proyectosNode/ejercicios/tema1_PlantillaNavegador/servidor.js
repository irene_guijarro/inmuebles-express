const http = require('http');
const fs = require('fs');

let atenderPeticion = (request, response) => {
    response.writeHead(200, {"Content-Type": "text/html"});
    let contenido = fs.readFileSync('./index.html', 'utf8');
    let navegador = request.headers['user-agent'];
    if (navegador.includes("Chrome")) {
        navegador = "Utilizas Google Chrome";
    }
    else {
        navegador = "Utilizas un navegador no reconocido";
    }

    contenido = contenido.replace('{saludo}', navegador);
    response.write(contenido);
    response.end();
}

http.createServer(atenderPeticion).listen(8080);