const http = require('http');
const fs = require('fs');
const socketio = require('socket.io');

let atenderPeticion = (request, response) => {
    if (request.url === '/index.html' || request.url === '/socket.io.js') {
        fs.createReadStream('.' + request.url).pipe(response);
    }
};

let servidor = http.createServer(atenderPeticion).listen(8080);
let io = socketio.listen(servidor);

io.on('connection', (socket) => {
    socket.emit('conectado');
    socket.on('enviar', (datos) => {
        io.emit('difundir', datos);
    })
});
