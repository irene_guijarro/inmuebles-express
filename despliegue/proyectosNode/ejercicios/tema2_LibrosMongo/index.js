const http = require("http");
const Autor = require("./modelo/autor");
const Libro = require("./modelo/libro");
const bdconfig = require("./modelo/bdconfig");

let atenderPeticion = (request, response) => {
  response.writeHead(200, { "Content-Type": "application/json" });
    if (request.url === "/autor" && request.method === "POST") {
        let body = [];
        request
        .on("data", chunk => {
            body.push(chunk);
        })
        .on("end", () => {
            body = Buffer.concat(body).toString();
            let autorJSON = JSON.parse(body);
            let nuevoAutor = new Autor({
            nombre: autorJSON.nombre,
            apellidos: autorJSON.apellidos,
            anyoNacimiento: autorJSON.anyoNacimiento
            });
            nuevoAutor
            .save()
            .then(resultado => {
                let respuesta = { error: false, resultado: resultado };
                response.end(JSON.stringify(respuesta));
            })
            .catch(error => {
                let respuesta = {
                error: true,
                mensajeError: "Error al añadir autor"
                };
                response.end(JSON.stringify(respuesta));
            });
        });
    } else if (request.url === '/libro' && request.method === 'POST') {
        let body = [];
        request.on('data', (chunk) => {
            body.push(chunk);
        }).on('end', () => {
            body = Buffer.concat(body).toString();
            let libroJSON = JSON.parse(body);
            let nuevoLibro = new Libro({
                titulo: libroJSON.titulo,
                autor: libroJSON.autor,
                precio: libroJSON.precio
            });
            nuevoLibro.save().then(resultado => {
                let respuesta = {error: false, resultado: resultado};
                response.end(JSON.stringify(respuesta));
            }).catch (error => {
                let respuesta = {error: true,
                    mensajeError:"Error al añadir libro"};
                response.end(JSON.stringify(respuesta));
            });
        });
    } else if (request.method === 'GET' && request.url === '/libro') {
        Libro.find().then(resultado => {
            let respuesta = {error: false, resultado: resultado};
            response.end(JSON.stringify(respuesta));
        }).catch(error => {
            let respuesta = {error: true, mensajeError: "Error al buscar los libros"};
            response.end(JSON.stringify(respuesta));
        })
    } else if (request.method === 'GET' && request.url.startsWith('/libro/')) {
        let id = request.url.split('/')[2];
        Libro.findById(id).populate('autor').then(resultado => {
            if (resultado) {
                let respuesta = {error: false, resultado: resultado};
                response.end(JSON.stringify(respuesta));
            } else {
                let respuesta = {error: true, mensajeError:"No se encuentra el libro"};
                response.end(JSON.stringify(respuesta));
            }
        }).catch(error => {
            let respuesta = {error: true, mensajeError: "Error al buscar el libro"}
            response.end(JSON.stringify(respuesta));
        });
    }
};

http.createServer(atenderPeticion).listen(8080);
