const mongoose = require("mongoose");

let LibroSchema = new mongoose.Schema({
    titulo: {
        type: String,
        required: true,
        minlength: 1,
        trim: true
    },
    autor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Autor",
        required: true
    },
    precio: Number
});

let Libro = mongoose.model("Libro", LibroSchema);
module.exports = Libro;
