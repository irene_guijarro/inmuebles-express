const mongoose = require("mongoose");

let AutorSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        minlength: 1,
        trim: true
    },
    apellidos: {
        type: String,
        required: true,
        minlength: 1,
        trim: true
    },
    anyoNacimiento: {
        type: Number,
        min: 1000,
        max: 2000
    }
});

let Autor = mongoose.model("Autor", AutorSchema);
module.exports = Autor;
