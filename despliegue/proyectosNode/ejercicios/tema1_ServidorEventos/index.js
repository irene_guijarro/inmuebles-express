const http = require('http');
let EventEmitter = require('events');

let eventos_config = require('./eventos_config');
let gestor_eventos = new EventEmitter();

let atenderPeticion = (request, response) => {
    response.writeHead(200, {"Content-Type": "text/plain"});
    gestor_eventos.emit(eventos_config.eventos.PETICION_HORA, response);
    response.end();

};

let mostrar_saludo = (response) => {
    let hora = new Date().getHours();
    if (hora >= 7 && hora <= 12)
        response.write("Buenos días");
    else if (hora >= 13 && hora <= 20)
        response.write("Buenas tardes");
    else
        response.write("Buenas noches");
}

gestor_eventos.on(eventos_config.eventos.PETICION_HORA, mostrar_saludo)
http.createServer(atenderPeticion).listen(8080);