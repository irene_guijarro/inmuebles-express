/* Ejercicio: desarrollar un servidor Node que dé respuesta al
   cliente JavaFX proporcionado. Se debe autenticar por token 
   al usuario que entra, para permitirle subir imágenes al servidor */

const jwt = require('jsonwebtoken');
const http = require('http');
const fs = require('fs');

// Palabra secreta para firmar los tokens
const secreto = 'secretoDAW';

// Array de usuarios, supuestamente extraído con alguna consulta
let usuarios = [
    {login: 'nacho', password: '1234'},
    {login: 'arturo', password: '5678'},
    {login: 'alex', password: '4321'},
    {login: 'julio', password: '8765'}
];

let validarUsuario = (login, password) => {
  let usuarioOk = usuarios.filter(
    usuario => usuario.login === login && usuario.password === password
  );
  if (usuarioOk.length > 0) {
    return generarToken(login);
  }
};

let generarToken = login => {
  let token = jwt.sign({ login: login }, secreto, { expiresIn: "1 minute" });
  return token;
};

let validarToken = (token) => {
    try {
        let resultado = jwt.verify(token, secreto);
        return resultado;
    } catch (e) {
        return undefined;
    }
}

let atenderPeticion = (request, response) => {
    // URL de prueba para verificar la conexión (no necesita autenticación)
    if (request.url === '/test') {
        let resultado = {
            error: false,
            mensajeError: "Prueba de test satisfactoria"
        };
        response.writeHead(200, {"Content-Type":"application/json"});
        response.write(JSON.stringify(resultado));
        response.end();
    } else if (request.url === '/login' && request.method === 'POST') {
        let body = [];
        let resultado = {};
        request.on('data', (chunk) => {
            body.push(chunk);
        }).on('end', () => {
            body = Buffer.concat(body).toString();
            response.writeHead(200, {"Content-Type":"application/json"});
            let usuario = JSON.parse(body);
            let token = validarUsuario(usuario.login, usuario.password);
            if (token) {
                resultado.error = false;
                resultado.token = token;
            } else {
                resultado.error = true;
                resultado.mensajeError = "Usuario o contraseña incorrectos";
            }
            response.write(JSON.stringify(resultado));
            response.end();
        })
    } else if (request.url === '/subir' && request.method === 'POST') {
        // Validar al usuario
        let token = request.headers['authorization'];
        let resultadoToken = validarToken(token);
        if (resultadoToken) {
            token = generarToken(resultadoToken.login); //Se renueva el token para dar 1 min mas de tiempo
            let body = [];
            let resultado = {};
            request.on('data', (chunk) => {
                body.push(chunk);
            }).on('end', () => {
                body = Buffer.concat(body).toString();
                let imagen = JSON.parse(body);
                let datos = Buffer.from(imagen.datos, 'base64');
                if (!fs.existsSync("./" + resultadoToken.login)) {
                    fs.mkdirSync("./" + resultadoToken.login);
                }
                fs.writeFileSync("./" + resultadoToken.login + "/" + imagen.nombreArchivo, datos);
                resultado.error = false;
                resultado.token = token;
                response.writeHead(200, {"Content-Type":"application/json"});
                response.write(JSON.stringify(resultado));
                response.end();
            });
        } else {
            resultado.error = true;
            resultado.mensajeError = "Usuario no autenticado - Permiso caducado";
            response.writeHead(200, {"Content-Type":"application/json"});
            response.write(JSON.stringify(resultado));
            response.end();
        }
    }
};

http.createServer(atenderPeticion).listen(8080);
