const express = require('express');
const http = require('http');
const fs = require('fs');
const socketio = require('socket.io');

let app = express();

let server = http.createServer(app);
let io = socketio.listen(server);

app.set('view engine', 'ejs');
app.use(express.static('./public'));

app.get('/', (req, res) => {
    res.render('index');
});

io.on('connection', (socket) => {
    socket.emit('conectado');
    socket.on('enviar', (datos) => {
        io.emit('difundir', datos);
    });
});

server.listen(8080);
