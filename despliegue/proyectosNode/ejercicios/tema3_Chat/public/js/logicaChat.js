let socket = io('http://localhost:8080');
       
let enviar = () => { 
    let  texto = document.getElementById('texto').value;
    let nick = document.getElementById('nick').value;
    socket.emit('enviar', {texto: texto, nick: nick}) 
}
socket.on('conectado', (datos) => {
    console.log('en script');
    let campoTexto = document.getElementById('texto');
    campoTexto.style.borderColor = "lightgreen";
    campoTexto.removeAttribute('placeholder');
    alert("Conectado");
});

socket.on('difundir', (datos) => {
    let chat = document.getElementById('chat');
    let li = document.createElement("li");
    li.innerText = datos.nick + ": " + datos.texto;
    chat.appendChild(li);
    let campoTexto = document.getElementById('texto');
    campoTexto.value = "";
});