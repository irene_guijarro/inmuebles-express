const express = require('express');
const bodyParser = require('body-parser');
const index = require('./routes/index');
const contactos = require('./routes/contactos');

let app = express();
app.set('vew engine', 'ejs');
app.use('/public', express.static('./public'));
app.use(bodyParser.json());
app.use('/index', index);

app.listen(8080);