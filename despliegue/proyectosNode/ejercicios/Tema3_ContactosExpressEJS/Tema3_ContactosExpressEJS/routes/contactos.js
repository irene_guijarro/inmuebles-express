const express = require('express');

let router = express.Router();

router.get('/', (res, req) => {
    Contacto.find().then(resultado => {
        res.render('lista_contactos', {contactos:resultado});
    }).catch(error => {
        res.render('lista_contactos', {contactos: []});
    });
});

router.get('/:id', (res, req) => {
    Contacto.findById(req.params.id).then(resultado => {
        if (resultado)
            res.render('ficha_contacto', {error: false, contacto: resultado});
        else
            res.render('ficha_contacto', {error: true, contacto: ''});
    }).catch(error => {
        
    })
});

module.exports = router;