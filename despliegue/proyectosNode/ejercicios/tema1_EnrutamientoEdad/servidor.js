const http = require('http');
const os = require('os');
const js = require('fs');

let atenderPeticion = (request, response) => {
    if (request.url === '/') {
        response.writeHead(200, {"Content-Type":"text/html"});
        response.write(fs.readFileSync('./index.html', 'utf8'));
    } else if (request.url == '/usuario') {
        response.writeHead(200, {"Content-Type":"text/plain"});
        response.write(os.userInfo().username);
    } else if (request.url == '/carpeta') {
        response.writeHead(200, {"Content-Type":"text/plain"});
        js.readdirSync('.').forEach(fichero => response.write(fichero + "\n"));
    }

    response.end();    
}

http.createServer(atenderPeticion).listen(8080);