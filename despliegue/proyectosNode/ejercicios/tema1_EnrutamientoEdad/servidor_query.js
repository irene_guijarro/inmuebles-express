//Ejercicio 5
const http = require('http');
const url = require('url');

let atenderPeticion = (request, response) => {
    response.writeHead(200, {"Content-Type":"text/plain"});
    if(request.url.startsWith("/saludo")) {
        let parametros = url.parse(request.url, true).query;
        let nombre = parametros.nombre;
        let edad = parametros.edad;
        response.write("Hola," + nombre);
        if (edad >= 18) {
            response.write(", eres mayor de edad");
        }
        else {
            response.write(", no eres mayor de edad");
        }    
    }
    response.end();
}

http.createServer(atenderPeticion).listen(8080);