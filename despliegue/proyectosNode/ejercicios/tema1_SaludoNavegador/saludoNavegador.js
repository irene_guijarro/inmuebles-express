const http = require('http');

let atenderPeticion = (request, response) => {
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.write("Bienvenido/a\n");
    let navegador = request.headers['user-agent'];
    if (navegador.includes("Chrome")) {
        navegador = "Chrome";
    }
    else {
        navegador = "Otro";
    }
    response.write("Tienes este navegador: " + navegador);
    response.end();
}
    
http.createServer(atenderPeticion).listen(8080);