const os = require('os');

let loginName = os.userInfo().username;
let esUsuario = (login) => loginName === login;

module.exports = {
    esUsuario: esUsuario,
    loginName: loginName
};