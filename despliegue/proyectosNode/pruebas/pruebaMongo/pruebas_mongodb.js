const { MongoClient, ObjectID } = require("mongodb");

MongoClient.connect("mongodb://localhost:27017/contactos", (error, bd) => {
  if (error) 
    return console.log("No se pudo conectar con la base de datos");
  console.log("Conectado al servidor MongoDB");
  bd.collection("contactos").insertOne(
    {
      nombre: "Irene",
      telefono: "605157532"
    },
    (error, resultado) => {
      if (error) 
          return console.log("Error al insertar documento");
      console.log(JSON.stringify(resultado.ops));
    }
  );
  bd.close();
});
