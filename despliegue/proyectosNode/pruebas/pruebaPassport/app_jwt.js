const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");
const { Strategy, ExtractJwt } = require("passport-jwt");
const jwt = require("jsonwebtoken");
const Usuario = require("./models/usuario");
const secreto = "secretoDAW";

let generarToken = id => {
  return jwt.sign({ id: id }, secreto, { expiresIn: "2 hours" });
};

mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/pruebapassport");

passport.use(
  new Strategy(
    {
      secretOrKey: secreto,
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
    },
    (payload, done) => {
      if (payload.id) {
        return done(null, { id: payload.id });
      } else {
        return done(new Error("Usuario incorrecto"), null);
      }
    }
  )
);

let app = express();
app.use(passport.initialize());
app.use(bodyParser.json());

app.get("/", (req, res) => {
  res.send("Bienvenido/a a la zona pública");
});

app.post("/login", (req, res) => {
  Usuario.findOne({ login: req.body.login, password: req.body.password })
    .then(resultado => {
      if (resultado) res.send({ ok: true, token: generarToken(resultado._id) });
      else res.send({ ok: false, mensajeError: "Usuario incorrecto" });
    })
    .catch(error => {
      res.send({ ok: false, mensajeError: "Usuario incorrecto" });
    });
});

app.get("/protegido", passport.authenticate("jwt", { session: false }), (req, res) => {
    res
      .status(200)
      .send("Bienvenido/a a la zona protegida " + JSON.stringify(req.user));
  }
);

app.listen(8080);
