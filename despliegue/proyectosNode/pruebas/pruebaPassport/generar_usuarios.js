const mongoose = require("mongoose");
const Usuario = require("./models/usuario");

mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/pruebapassport");
Usuario.collection.drop();

let usu1 = new Usuario({
  login: "nacho",
  password: "1234"
});
usu1.save();
let usu2 = new Usuario({
  login: "arturo",
  password: "5678"
});
usu2.save();
