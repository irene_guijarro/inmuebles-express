const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");
const Usuario = require("./models/usuario");
const LocalStrategy = require("passport-local").Strategy;

mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/pruebapassport");

let app = express();
app.use(passport.initialize());
app.use(bodyParser.urlencoded({ extended: false }));

passport.use(
  new LocalStrategy((login, password, done) => {
    Usuario.findOne({ login: login, password: password })
      .then(resultado => {
        if (resultado) return done(null, resultado);
        else return done(null, false);
      })
      .catch(error => {
        return done(error, false);
      });
  })
);

app.post("/", passport.authenticate("local", {session: false, failureRedirect: "/login"}),(req, res) => {
    res.redirect("/ok/" + req.user.login);
  }
);

app.get("/ok/:login", (req, res) => {
  res.status(200).send("Bienvenido/a " + req.params.login);
});

app.get("/login", (req, res) => {
  res.status(401).send("Usuario incorrecto");
});

app.listen(8080);
