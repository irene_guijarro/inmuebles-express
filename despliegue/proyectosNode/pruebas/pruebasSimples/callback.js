//2.1 Callbacks. 

//Primero saca Hola y después el mensaje de finalizado callback.
setTimeout(function() {console.log("Finalizado callback");}, 2000);
console.log("Hola");

//Si se comenta el callback() no sacará "Finalizando callback"
let saludar = (callback) => {
    console.log("Hola");
    callback();
}
saludar(function() {console.log("Fializando callback");})