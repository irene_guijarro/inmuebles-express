const http = require('http');

let atenderPeticion = (request, response) => {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.write("El comando recibido es: " + request.method);
    response.end();
};


http.createServer(atenderPeticion).listen(8080);