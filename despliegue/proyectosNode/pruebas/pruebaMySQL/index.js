const http = require("http");
const Contacto = require("./modelo/contacto");

let atenderPeticion = (request, response) => {
  if (request.method === "GET" && request.url === "/contactos") {
    response.writeHead(200, { "Content-Type": "application/json" });
    Contacto.listarContactos()
      .then(resultado => {
        response.end(JSON.stringify(resultado));
      })
      .catch(error => {
        response.end(JSON.stringify([]));
      });
  } else if (request.method === "POST" && request.url === "/contacto") {
    response.writeHead(200, { "Content-Type": "application/json" });
    let body = [];
    request
      .on("data", chunk => {
        body.push(chunk);
      })
      .on("end", () => {
        body = Buffer.concat(body).toString();
        let contactoJSON = JSON.parse(body);
        let contacto = new Contacto(contactoJSON);
        contacto
          .crear()
          .then(resultado => {
            let respuesta = { error: false, nuevoId: resultado.insertId };
            response.end(JSON.stringify(respuesta));
          })
          .catch(error => {
            let respuesta = {
              error: true,
              mensajeError: "No se pudo crear el contacto"
            };
            response.end(JSON.stringify(respuesta));
          });
      });
  }
};

http.createServer(atenderPeticion).listen(8080);
