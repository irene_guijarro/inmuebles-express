const conextion = require("./bdconfig");

module.exports = class Contacto {
  constructor(contactoJSON) {
    this.id = contactoJSON.id;
    this.nombre = contactoJSON.nombre;
    this.telefono = contactoJSON.telefono;
  }

  static listarContactos() {
    return new Promise((resolve, reject) => {
      conexion.query("SELECT * FROM contactos", (error, resultado, campos) => {
        if (error) return reject(error);
        else resolve(resultado.map(cJSON => new Contacto(cJSON)));
      });
    });
  }

  crear() {
    return new Promise((resolve, reject) => {
      let datos = { nombre: this.nombre, telefono: this.telefono };
      conexion.query(
        "INSERT INTO contactos SET ?",
        datos,
        (error, resultado, campos) => {
          if (error) return reject(error);
          else resolve(resultado);
        }
      );
    });
  }
};
