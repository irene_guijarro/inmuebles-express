const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const Contacto = require('./models/contacto');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/contactos', {useMongoClient: true});

let app = express();
app.use(bodyParser.json());

app.get('/contactos', (req, res) => {
    Contacto.find().then(resultado => {
        res.send(resultado);
    }).catch(error => {
        res.send([]);
    });
});

app.get('/contactos/:id', (req, res) => {
    Contacto.findById(req.params.id).then(resultado => {
        if (resultado)
            res.send({error: false, resultado: resultado});
        else
            res.send({error: true, mensajeError: "No se han encontrado contactos"});
    }).catch(error => {
        res.send({error: true, mensajeError: "Error buscando el contacto"});
    });
});

app.post('/contactos', (req, res) => {
    let nuevoContacto = new Contacto(req.body);
    nuevoContacto.save().then(resultado => {
        res.send({error: false, resultado: resultado});
    }).catch(error => {
        res.send({error: true, mensajeError: "Error al insertar el contacto"});
    });
});



app.listen(8080);