const express = require('express');
/* const usuarios = require('./routes/usuarios');
const principal = require('./routes/index'); */
const usuarios = require('./controllers/usuarios');
const principal = require('./controllers/index');

let app = express();
usuarios(app);
principal(app);
/* app.get('/bienvenida', (req, res) => {
    res.send('Hola, bienvenido/a');
}); */
/* 
app.use('/usuarios', usuarios);
app.use('/', principal); */

app.listen(8080);