const express = require('express');

let router = express.Router();

router.get('/', (req, res) => {
    console.log('Listado de usuarios');
});

router.post('/', (req, res) => {
    console.log('Inserción de usuario');
});

module.exports = router;