const express = require('express');

let router = express.Router();

router.use((req, res, next) => {
    console.log(new Date().toLocaleString());
    next();
})

router.get('/', (req, res) => {
    console.log('Página de inicio');
});

router.get('/noticias', (req, res) => {
    console.log('Noticias');
})

module.exports = router;