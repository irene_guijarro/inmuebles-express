const express = require('express');
const fileUpload = require('express-fileupload');

let app = express();
app.use(fileUpload());
app.use(express.static('./public'));

app.post('/upload', (req, res) => {
    console.log("Enviado por: ", req.body.usuario);
    if (!req.files)
        return res.status(400).send("No se ha especificado imagen");
    req.files.fichero.mv('./uploads/' + req.files.fichero.name, error => {
        if (error) {
            console.log(error);
            res.status(500).send("ERROR subiendo imagen");
        }
        res.send("Archivo subido");
    });
});

app.listen(8080);