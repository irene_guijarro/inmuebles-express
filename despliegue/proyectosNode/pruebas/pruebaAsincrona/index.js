// 2.2 Conectar con webs remotas, modo síncrono y asíncrono.
const request = require('request');
// const srequest = require('sync-request');

const urls = [
    {nombre: 'stackoverflow', url: "https://stackoverflow.com/"},
    {nombre: 'google', url: "http://google.com"},
    {nombre: 'conselleria', url: "http://www.ceice.gva.es"},
    {nombre: 'meneame', url: "https://www.meneame.net/"}
]

// let llamdaSincrona = () => {
//     urls.forEach(url => {
//         srequest('GET', url.url);
//         console.log("Finalizado", url.nombre);
//     });
//     console.log("Proceso completado");
// };

// llamdaSincrona();

let llamadaAsincrona = () => {
    urls.forEach(url => {
        request(url.url, (err, resp, body) => {
            console.log("Finalizado", url.nombre);
        });
    });
    console.log('Proceso completado');
};

llamadaAsincrona();