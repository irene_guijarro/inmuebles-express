const jwt = require('jsonwebtoken');
const secreto = 'secretoDAW';

let usuarios = [
  { login: "nacho", password: "1234" },
  { login: "arturo", password: "5678" },
  { login: "alex", password: "4321" },
  { login: "julio", password: "8765" }
];
let validarUsuario = (login, password) => {
  let usuarioOk = usuarios.filter(
    usuario => usuario.login === login && usuario.password === password
  );
  if (usuarioOk.length > 0) {
    return generarToken(login);
  }
};
let generarToken = login => {
  let token = jwt.sign({ login: login }, secreto, { expiresIn: "1 minute" });
  return token;
};
            
let prueba = validarUsuario('nacho', '1234');
console.log(prueba);

let validarToken = token => {
  try {
    let resultado = jwt.verify(token, secreto);
    return resultado;
  } catch (e) {}
};

let resultado = validarToken(prueba);
if (resultado) {
    console.log("Usuario validado:", resultado.login);
    let tokenRenovado = generarToken(resultado.login);
    console.log("Nuevo token:\n", tokenRenovado);
}