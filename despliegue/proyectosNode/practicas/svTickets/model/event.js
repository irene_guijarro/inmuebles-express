const conexion = require('./bdconfig');
const express = require('express');
const User = require('../model/user');

module.exports = class Event
{
    constructor(eventJSON) {
        this.id = eventJSON.id;
        this.creator = eventJSON.creator;
        this.title = eventJSON.title;
        this.description = eventJSON.description;
        this.date =  eventJSON.date.toLocaleString('es-ES');
        this.price = eventJSON.price;
        this.lat = eventJSON.lat;
        this.lng = eventJSON.lng;
        this.address = eventJSON.address;
        this.image = eventJSON.image;
    }

    static listarEventos() {
        return new Promise ( (resolve, reject) => {
            conexion.query("SELECT * FROM event;", 
                (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else {
                        resolve(resultado);
                    }
                });
        });
    }

    static getUserEvents(userId) {
        return new Promise (( resolve, reject) => {
            conexion.query("SELECT * FROM event WHERE creator=" + userId + ";",
                (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else
                        resolve(resultado);
                });
        });
    }

    static getUserAttendEvents(userId) {
        return new Promise ((resolve, reject) => {
            conexion.query("SELECT * FROM event WHERE event.id IN (SELECT event FROM user_attend_event WHERE user = " + userId + ");",
                (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else {
                        resolve(resultado);
                    }
                })
        })
    }

    static attendEvent(userId, eventId, numTickets) {
        let datos = {
            user: userId,
            event: eventId,
            tickets: numTickets
        };

        return new Promise((resolve, reject) => {
            conexion.query("INSERT INTO user_attend_event SET ?", datos,
                (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else {
                        resolve(resultado);
                    }
                })
        })
    }

    static getEventById(id) {
        return new Promise ((resolve, reject) => {
            conexion.query("SELECT * FROM event WHERE id=" + id + ";", 
                (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else {
                        resolve(resultado[0]);
                    }
                });
        });
    }

    static delete(eventId, userId) {
        return new Promise ((resolve, reject) => {
            conexion.query("DELETE FROM event WHERE id=" + eventId + " AND creator=" + userId + ";",
                (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else {
                        if (resultado.affectedRows === 0) {
                            resolve({ok: false, errorMessage: 'User is not the creator, dennied operation'});
                        } else {
                            resolve({ok: true, affectedRows: resultado.affectedRows});
                        }
                    }
                })
        })
    }

    crear() {
        return new Promise((resolve, reject) => {
            let datos = {creator: this.creator, title: this.title, description: this.description, 
                date: this.date, price: this.price, lat: this.lat, lng: this.lng, address: this.address, 
                image: this.image};
            conexion.query("INSERT INTO event SET ?", datos,
                (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else {
                        resolve(resultado);
                    }
                });
        });
    }


    update() {
        return new Promise ((resolve, reject) => {
            let datos = {creator: this.creator.id, title: this.title, description: this.description,
                date: this.date, price: this.price, lat: this.lat, lng: this.lng, address: this.address};
            conexion.query("UPDATE event SET ? WHERE id=" + this.id + ";", datos, 
                (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else {
                        resolve(resultado);
                    }
                });
            });
    }

    getDistance(user) {
        return new Promise ((resolve, reject) => {
            conexion.query(" SELECT `haversine`(" + user.lat + ", " + user.lng + ", " + this.lat + ", " + this.lng + ")AS `haversine`;"
            , (error, resultado, campos) => {
                if (error)
                    return reject(error);
                else {
                    resolve(resultado[0]);
                }
            });
        });
    }

    static getAttendes(eventId) {
        return new Promise ((resolve, reject) => {
            conexion.query("SELECT * FROM user WHERE user.id IN (SELECT user FROM user_attend_event WHERE event = " + eventId + ");",
                (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else {
                        resolve(resultado);
                    }
                })
        })
    }
}