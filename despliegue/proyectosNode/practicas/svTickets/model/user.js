const conexion = require('./bdconfig');
const jwt = require('jsonwebtoken');
const md5 = require('md5');
const secreto = 'daw';

module.exports = class Usuario {

    static registrarUsuario(usuarioJSON) {
        let passEncriptada = md5(usuarioJSON.password);
        let datos = {name: usuarioJSON.name, email: usuarioJSON.email, password: passEncriptada, 
            avatar: usuarioJSON.avatar, lat: usuarioJSON.lat, lng: usuarioJSON.lng};
        return new Promise( (resolve, reject) => {
            conexion.query("INSERT INTO user SET ?", datos,
            (error, resultado, campos) => {
                if (error)
                    return reject(error);
                else 
                    resolve(resultado.insertId);
            });
        }); 
    }

    static registrarUsuarioGoogle(usuarioJSON) {
        let datos = {id_google: usuarioJSON.id_google, name: usuarioJSON.name, email: usuarioJSON.email,
            avatar: usuarioJSON.avatar, lat: 0, lng: 0};
        let querySelect = "SELECT * FROM user WHERE id_google = '" + datos.id_google + "';";
        let respuestaGoogle;
        return new Promise((resolve, reject) => {
            conexion.query(querySelect, (error, resultado, campos) => {
                if (error) {
                    respuestaGoogle = {
                        error: true,
                        token: false
                    };
                    reject(respuestaGoogle);
                }
                if (resultado.length === 0) {
                    // Usuario no está en la bbdd, crearlo, generar token y devolverlo
                    return new Promise((resolve, reject) => {
                        conexion.query("INSERT INTO user SET ?", datos, (error, resultado, campos) => {
                            if (error) {
                                respuestaGoogle = {
                                    error: true,
                                    token: false
                                };
                                reject(respuestaGoogle);
                            }
                            else {
                                let token = this.generarToken(datos.email, datos.id_google);
                                respuestaGoogle = {
                                    error: false,
                                    token: token
                                };
                                resolve(respuestaGoogle);
                            }
                        });
                    });
                }
                else {
                    // generar token y devolverlo
                    let token = this.generarToken(datos.email, datos.id_google);
                    respuestaGoogle = {
                        error: false,
                        token: token
                    };
                    resolve(respuestaGoogle);
                }
            })
        })
    }

    static registrarUsuarioFacebook(usuarioJSON) {
        // Le doy un valor al email porque en mi cuenta de facebook no está (es muy vieja), asi que la llamada no lo devuelve
        let datos = {id_facebook: usuarioJSON.id_facebook, name: usuarioJSON.name, email: usuarioJSON.email | 'iguijarro44@gmail.com',
            avatar: usuarioJSON.avatar, lat: 0, lng: 0};
        let querySelect = "SELECT * FROM user WHERE id_facebook = '" + datos.id_facebook + "';";
        let respuestaFacebook;
        return new Promise((resolve, reject) => {
            conexion.query(querySelect, (error, resultado, campos) => {
                if (error) {
                    respuestaFacebook = {
                        error: true,
                        token: false
                    };
                    reject(respuestaFacebook);
                }
                else if (resultado.length === 0) {
                    // Usuario no está en la bbdd, crearlo, generar token y devolverlo
                    return new Promise((resolve, reject) => {
                        conexion.query("INSERT INTO user SET ?", datos, (error, resultado, campos) => {
                            if (error) {
                                respuestaFacebook = {
                                    error: true,
                                    token: false
                                };
                                reject(respuestaFacebook);
                            }
                            else {
                                let token = this.generarToken(datos.email, datos.id_facebook);
                                respuestaFacebook = {
                                    error: false,
                                    token: token
                                };
                                resolve(respuestaFacebook);
                            }
                        });
                    });
                }
                else {
                    // generar token y devolverlo
                    let token = this.generarToken(datos.email, datos.id_facebook);
                    respuestaFacebook = {
                        error: false,
                        token: token
                    };
                    resolve(respuestaFacebook);
                }
            })
        })
    }
    
    static validarUsuario(login, password) {
        let passEncriptada = md5(password);
        let query = "SELECT * FROM user WHERE email = '" + login + "' AND password = '" + passEncriptada + "';";
        return new Promise( (resolve, reject) => {
            conexion.query(query,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    }
                        
                    else {
                        if (resultado.length == 1) {
                            let email = resultado[0].email;
                            let id = resultado[0].id;
                            let respuesta = {
                                error:false,
                                token:this.generarToken(email, id)
                            };
                            resolve(respuesta);
                        }
                        else {
                            let respuesta = {
                                error:true,
                                mensajeError:"El usuario o la password son incorrectos"
                            };
                            resolve(respuesta);
                        }
                    }
                });
        });
    }

    static updateLocation(login, newLat, newLng) {
        return new Promise ((resolve, reject) => {
            conexion.query("UPDATE user SET lat=" + newLat + " , lng=" + newLng + " WHERE email='" + login + "' ;",
                (error, resultado, campos) => {
                    if (error)
                        reject(error)
                    else
                        resolve(resultado);
                })
        })
    }

    static getUserById(id) {
        return new Promise ((resolve, reject) => {
            conexion.query("SELECT * FROM user WHERE id=" + id + " OR id_google=" + id + " OR id_facebook=" + id + ";", 
                (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else {
                        resolve(resultado[0]);
                    }
                });
        });
    }

    static updateMe(name, email, userId) {
        return new Promise ((resolve, reject) => {
            let datos = {name: name, email: email};
            conexion.query("UPDATE user SET ? WHERE id=" + userId + ";", datos, 
                (error, resultado, campos) => {
                    console.log(campos);
                    if (error)
                        return reject(error);
                    else {
                        resolve(resultado);
                    }
                });
            });
    }

    static updateAvatar(image, userId) {
        return new Promise ((resolve, reject) => {
            let datos = {avatar: image};
            conexion.query("UPDATE user SET ? WHERE id=" + userId + ";", datos, 
                (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else {
                        resolve(resultado);
                    }
                });
            });
    }

    static updatePassword(password, userId) {
        let passEncriptada = md5(password);
        return new Promise ((resolve, reject) => {
            let datos = {password: passEncriptada};
            conexion.query("UPDATE user SET ? WHERE id=" + userId + ";", datos, 
                (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else {
                        resolve(resultado);
                    }
                });
            });
    }

    static generarToken(login, id) {
        let token = jwt.sign({login: login, id: id}, secreto, {expiresIn:"30 minutes"});
        return token;
    }

    static validarToken(token) {
        try {
            token = token.replace("Bearer ", "");
            let resultado = jwt.verify(token, secreto);
            return resultado;
        } catch (e) {
            return "Token no valido";
        }
    }
}