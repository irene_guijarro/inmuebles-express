const express = require('express');
const Event = require('../model/event');
const User = require('../model/user');
const Imagen = require('../helpers/imagen');
const base64Img = require('base64-img');

let router = express.Router();

router.use((req, res, next) => {
    let token = req.headers['authorization'];
    let respuesta = User.validarToken(token);
    if (respuesta === "Token no valido") {
        respuesta = {
            ok: false,
            mensajeError: 'Acceso no autorizado'
        }
        res.status(403);
        res.send(respuesta);
    }
    else {
        next();
    }
});

router.get('/', (req, res) => {
    let token = req.headers['authorization'];
    let currentUserId = User.validarToken(token).id;
    let respuesta;
    let currentUser;
    User.getUserById(currentUserId).then((currUser) => {
        currentUser = currUser;
        Event.listarEventos().then((response) => {
            if (response.error) {
                respuesta = {
                    ok: false,
                    errorMessage: 'Error getting events'
                };
                res.send(respuesta);
            }
            else {
                let promesas = [];
                response.forEach(event => {
                    promesas.push(new Promise((resolve) => {
                        User.getUserById(event.creator).then((resp) => {
                            if (event.creator === currentUserId) {
                                event.mine = true;
                            }
                            else
                                event.mine = false;
                            event.creator = resp;
                            let eventObj = new Event(event);
                            eventObj.getDistance(currUser).then((distance) => {
                                event.distance = distance.haversine;
                                resolve();
                            });
                        });
                       }));
                });
                Promise.all(promesas).then((reso) => {
                    respuesta = {
                        ok: true,
                        events: response
                    };
                    res.send(respuesta);
                });
            }
        }).catch(error => {
            respuesta = {
                ok: false,
                errorMessage: error
            };
            res.send(respuesta);
        });
    });
});

router.post('/', (req, res) => {
    let token = req.headers['authorization'];
    let creatorId = User.validarToken(token).id;
    let respuesta;

    Imagen.guardarImagen(req.body.image).then((response) => {
        req.body.image = response;
        let eventoJSON = req.body;
        eventoJSON.creator = creatorId;
        let evento = new Event(eventoJSON);
        evento.crear().then((response) => {
            if (response.error) {
                respuesta = {
                    ok: false,
                    errorMessage: "Error creating the event"
                };
                res.send(respuesta);
            }
            else {
                respuesta = {
                    ok: true,
                    eventId: response.insertId
                }
                res.send(respuesta);
            }
        }).catch(error => {
            respuesta = {
                ok: false,
                errorMessage: 'Server error creating event'
            };
            res.send(respuesta);
        });        
    });    
});

router.get('/mine', (req, res) => {
    let token = req.headers['authorization'];
    let currentUserId = User.validarToken(token).id;
    let respuesta;
    let currentUser;
    User.getUserById(currentUserId).then((currUser) => {
        currentUser = currUser;
        Event.getUserEvents(currentUserId).then((response) => {
            if (response.error) {
                respuesta = {
                    ok: false,
                    errorMessage: 'Error getting events'
                };
                res.send(respuesta);
            }
            else {
                let promesas = [];
                response.forEach(event => {
                    promesas.push(new Promise((resolve) => {
                        User.getUserById(event.creator).then((resp) => {
                            if (event.creator === currentUserId) {
                                event.mine = true;
                            }
                            else
                                event.mine = false;
                            event.creator = resp;
                            let eventObj = new Event(event);
                            eventObj.getDistance(currUser).then((distance) => {
                                event.distance = distance.haversine;
                                resolve();
                            });
                        });
                       }));
                });
                Promise.all(promesas).then((reso) => {
                    respuesta = {
                        ok: true,
                        events: response
                    };
                    res.send(respuesta);
                });
            }
        }).catch(error => {
            respuesta = {
                ok: false,
                errorMessage: error
            };
            res.send(respuesta);
        });
    });
});

router.get('/attend', (req, res) => {
    let token = req.headers['authorization'];
    let currentUserId = User.validarToken(token).id;
    let respuesta;
    let currentUser;
    User.getUserById(currentUserId).then((currUser) => {
        currentUser = currUser;
        Event.getUserAttendEvents(currentUserId).then((response) => {
            if (response.error) {
                respuesta = {
                    ok: false,
                    errorMessage: 'Error getting events'
                };
                res.send(respuesta);
            }
            else {
                let promesas = [];
                response.forEach(event => {
                    promesas.push(new Promise((resolve) => {
                        User.getUserById(event.creator).then((resp) => {
                            if (event.creator === currentUserId) {
                                event.mine = true;
                            }
                            else
                                event.mine = false;
                            event.creator = resp;
                            let eventObj = new Event(event);
                            eventObj.getDistance(currUser).then((distance) => {
                                event.distance = distance.haversine;
                                resolve();
                            });
                        });
                       }));
                });
                Promise.all(promesas).then((reso) => {
                    respuesta = {
                        ok: true,
                        events: response
                    };
                    res.send(respuesta);
                });
            }
        }).catch(error => {
            respuesta = {
                ok: false,
                errorMessage: error
            };
            res.send(respuesta);
        });
    });
});

router.post('/attend/:id', (req, res) => {
    let eventId = req.params.id;
    let numTickets = req.body.numTickets;
    let token = req.headers['authorization'];
    let userId = User.validarToken(token).id;
    let respuesta;

    Event.attendEvent(userId, eventId, numTickets).then((resolve) => {
            respuesta = {
                ok: true,
                result: resolve
            };
            res.send(respuesta);
        }).catch(error => {
            respuesta = {
                ok: false,
                errorMessage: error
            };
            res.send(respuesta);
        })
});

router.delete('/:id', (req, res) => {
    let eventId = req.params.id;
    let token = req.headers['authorization'];
    let userId = User.validarToken(token).id;
    let respuesta;

    Event.delete(eventId, userId).then((response) => {
        res.send(response);
    }).catch(error => {
        respuesta = {
            ok: false,
            errorMessage: error
        };
        res.send(respuesta);
    });
});

// Modify events
router.put('/:id', (req, res) => {
    let eventId = req.params.id;
    let eventJSON = req.body;
    let event = new Event(eventJSON);
    let respuesta;

    event.update().then((response) => {
        if (response.error) {
            respuesta = {
                ok: false,
                errorMessage: 'Error updating the event'
            };
            res.send(respuesta);
        } else {
            respuesta = {
                ok: true,
                event: eventJSON
            };
            res.send(respuesta);
        }
    }).catch(error => {
        respuesta = {
            ok: false,
            errorMessage: error
        };
        res.send(respuesta);
    });
});

router.get('/:id', (req, res) => {
    let respuesta;
    let id = req.params.id;
    let token = req.headers['authorization'];
    let currentUserId = User.validarToken(token).id;
    let currentUser;
    User.getUserById(currentUserId).then((currUser) => {
        currentUser = currUser;
        Event.getEventById(id).then(response => {
            if (response.error) {
                respuesta = {
                    ok: false,
                    errorMessage: "Error getting the event"
                };
                res.send(respuesta);
            }
            else {
                User.getUserById(response.creator).then((resp) => {
                    if (response.creator === currentUserId) {
                        response.mine = true;
                    }
                    else
                        response.mine = false;
                    response.creator = resp;
                    let event = new Event(response);
                    event.getDistance(currentUser).then((distance) => {
                        response.distance = distance.haversine;
                        respuesta = {
                            ok: true,
                            event: response
                        };
                        res.send(respuesta);
                    });
                });
            }
        }).catch(error => {
            respuesta = {
                ok: false,
                errorMessage: error
            };
            res.send(respuesta);
        });
    });
    
});

module.exports = router;