const express = require('express');
const base64Img = require('base64-img');
const https = require('https');
const fs = require('fs');
const User = require('../model/user');
const Imagen = require('../helpers/imagen');

let router = express.Router();

router.post('/register', (req, res) => {
    let avatar = Imagen.guardarImagenUsuario(req.body.avatar);
    req.body.avatar = avatar;
    let userJSON = req.body;
    let respuesta;
    User.registrarUsuario(userJSON).then((response) => {
        if (!response.error) {
            respuesta = {
                ok: true,
                result: { id: response }
            };
        }
        else {
            respuesta = {
                ok: false,
                errorMessage: 'Error, ' +  response.error
            };
        }
        res.send(respuesta);
    }).catch((error) => {
        respuesta = {
            ok: false,
            errorMessage: 'Error registering user, ' + error
        };
        res.send(respuesta);
    });
});

router.post('/login', (req, res) => {
    console.log('login');
    let respuesta;
    let user = {
        login: req.body.login,
        password: req.body.password,
        lat: req.body.lat,
        lng: req.body.lng
    };
    User.validarUsuario(user.login, user.password).then((response) => {
        if (!response.error) {
            User.updateLocation(user.login, user.lat, user.lng).then((respu) => {
                respuesta = {
                    ok: true,
                    token: response.token
                };
                console.log(response);
                res.send(respuesta);
            }).catch(error => {
            });
        }
        else {
            respuesta = {
                ok: false,
                errorMessage: response.mensajeError
            };
            console.log(response);
            res.send(respuesta);
        }
    }).catch(error => {
        respuesta = {
            ok: false,
            errorMessage: error
        };
        res.send(respuesta);
    })
});

router.get('/token', (req, res) => {
    let token = req.headers['authorization'];
    let respuesta = User.validarToken(token);
    if (respuesta === "Token no valido") {
        respuesta = {
            ok: false
        };
    }
    else {
        respuesta = {
            ok: true
        };
    }
    res.send(respuesta);
});

// Auth with Google
router.get('/google', (req, res) => {
    let token = req.headers['authorization'].replace('Bearer ', '');
    let respuesta;

    https.request('https://www.googleapis.com/plus/v1/people/me?access_token=' + token)
    .on('response', function(respu) {
        let body = '';
        respu.on('data', function (chunk) {
            body += chunk
        }).on('end', function() {
            let datos = JSON.parse(body);
            Imagen.guardarImagenGoogleOFb(datos.image.url, datos.name.givenName);
            let usuarioGoogle = {
                id_google: datos.id,
                name: datos.name.givenName,
                email: datos.emails[0].value,
                avatar: datos.name.givenName + ".png"
            };
            User.registrarUsuarioGoogle(usuarioGoogle).then((respuestaGoogle) => {
                if (respuestaGoogle.error) {
                    respuesta = {
                        ok: false,
                        errorMessage: 'Error with Google auth'
                    };
                }
                else {
                    respuesta = {
                        ok: true,
                        token: respuestaGoogle.token
                    };
                }
                res.send(respuesta);
            }).catch(error => {
                respuesta = {
                    ok: false,
                    errorMessage: 'Error with Google auth'
                };
                res.send(respuesta);
            });
        });
    })
    .end();
});

// Auth with Facebook
router.get('/facebook', (req, res) => {
    let token = req.headers['authorization'].replace('Bearer ', '');
    let respuesta;
    
    https.request('https://graph.facebook.com/v2.11/me?fields=id,name,email,picture&access_token=' + token)
    .on('response', function(resp) {
        let body = '';
        resp.on('data', function(chunk) {
            body += chunk
        })
        .on('end', function() {
            let datos = JSON.parse(body);
            Imagen.guardarImagenGoogleOFb(datos.picture.data.url, datos.name.split(' ')[0]);
            let usuarioFacebook = {
                id_facebook: datos.id,
                name: datos.name,
                email: datos.email,
                avatar: datos.name.split(' ')[0] + "_facebook.png"
            };
            User.registrarUsuarioFacebook(usuarioFacebook).then((respuestaFacebook) => {
                if (respuestaFacebook.error) {
                    respuesta = {
                        ok: false,
                        errorMessage: 'Error with Google auth'
                    };
                }
                else {
                    respuesta = {
                        ok: true,
                        token: respuestaFacebook.token
                    };
                }
                res.send(respuesta);
            }).catch(error => {
                respuesta = {
                    ok: false,
                    errorMessage: 'Error with Google auth'
                };
                res.send(respuesta);
            });
        });
    })
    .end();
});

module.exports = router;