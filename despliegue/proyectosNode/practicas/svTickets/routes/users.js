const express = require('express');
const Event = require('../model/event');
const User = require('../model/user');
const Imagen = require('../helpers/imagen');
const base64Img = require('base64-img');

let router = express.Router();

router.use((req, res, next) => {
    let token = req.headers['authorization'];
    let respuesta = User.validarToken(token);
    if (respuesta === "Token no valido") {
        respuesta = {
            ok: false,
            mensajeError: 'Acceso no autorizado'
        }
        res.status(403);
        res.send(respuesta);
    }
    else {
        next();
    }
});

// Ficha del usuario actual
router.get('/me', (req, res) => {
    let token = req.headers['authorization'];
    let userId = User.validarToken(token).id;
    let respuesta;

    User.getUserById(userId).then((response) => {
        if (response.error) {
            respuesta = {
                ok: false,
                errorMessage: 'Error getting the user data'
            };
            res.send(respuesta);
        }
        else {
            respuesta = {
                ok: true,
                user: response
            };
            res.send(respuesta);
        }
    }).catch(error => {
        respuesta = {
            ok: false,
            errorMessage: error
        };
        res.send();
    });
});

// Ficha de un usuario concreto
router.get('/:id', (req, res) => {
    let userId = req.params.id;
    let respuesta;

    User.getUserById(userId).then((response) => {
        if (response.error) {
            respuesta = {
                ok: false,
                errorMessage: 'Error getting the user data'
            };
            res.send(respuesta);
        }
        else {
            respuesta = {
                ok: true,
                user: response
            };
            res.send(respuesta);
        }
    }).catch(error => {
        respuesta = {
            ok: false,
            errorMessage: error
        };
        res.send();
    });
});

// Usuarios que acuden a un evento
router.get('/event/:id', (req, res) => {
    let eventId = req.params.id;
    let respuesta;
    Event.getAttendes(eventId).then(users => {
        if (users.error) {
            respuesta = {
                ok: false,
                errorMessage: 'Error getting the attendees for the event'
            };
            res.send(respuesta);
        }
        else {
            respuesta = {
                ok: true,
                users: users
            };
            res.send(respuesta);
        }
    }).catch(error => {
        respuesta = {
            ok: false,
            errorMessage: error
        };
        res.send(respuesta);
    });
});

// Modify name and email
router.put('/me', (req, res) => {
    let name = req.body.name;
    let email = req.body.email;
    let token = req.headers['authorization'];
    let userId = User.validarToken(token).id
    let respuesta;
    console.log(userId);
    User.updateMe(name, email, userId).then((response) => {
        if (response.error) {
            respuesta = {
                ok: false,
                errorMessage: 'Error updating the user'
            };
            res.send(respuesta);
        } else {
            respuesta = {
                ok: true,
                user: userId
            };
            res.send(respuesta);
        }
    }).catch(error => {
        respuesta = {
            ok: false,
            errorMessage: error
        };
        res.send(respuesta);
    });
});

// Modify avatar
router.put('/me/avatar', (req, res) => {
    let token = req.headers['authorization'];
    let userId = User.validarToken(token).id
    let respuesta;

    Imagen.guardarImagenUsuario(req.body.avatar).then((nombreImagen) => {
        req.body.avatar = nombreImagen;
        User.updateAvatar(req.body.avatar, userId).then((response) => {
            if (response.error) {
                resuesta = {
                    ok: false,
                    errorMessage: 'Error updating avatar'
                };
                res.send(respuesta);
            } else {
                respuesta = {
                    ok: true,
                    user: userId
                };
                res.send(respuesta);
            }
        }).catch(error => {
            respuesta = {
                ok: false,
                errorMessage: 'Server error updating avatar'
            }
        });
    }).catch(error => {
        respuesta = {
            ok: false,
            errorMessage: 'Server error processing avatar'
        };
        res.send(respuesta);
    });
});

// Modify password
router.put('/me/password', (req, res) => {
    let password = req.body.password;
    let token = req.headers['authorization'];
    let userId = User.validarToken(token).id
    let respuesta;
    User.updatePassword(password, userId).then((response) => {
        if (response.error) {
            respuesta = {
                ok: false,
                errorMessage: 'Error updating the password'
            };
            res.send(respuesta);
        } else {
            respuesta = {
                ok: true,
                user: userId
            };
            res.send(respuesta);
        }
    }).catch(error => {
        respuesta = {
            ok: false,
            errorMessage: error
        };
        res.send(respuesta);
    });
});

module.exports = router;