const http = require('http');
const modelo = require('./modelo');

let listarTareas = () => {
    return JSON.stringify(modelo.tareas.listarTareas());
}

let fichaTarea = (id) => {
    let resultado = {};
    if ((modelo.tareas.buscarTareaPorId(id) === false)) {
            resultado.error = true;
            resultado.mensajeError = "La tarea con ese id no existe";
    }
    else {
        resultado.error = false;
        resultado.mensajeError = "";
        resultado.tarea = modelo.tareas.buscarTareaPorId(id);
    }
    return JSON.stringify(resultado);
}

let listarTareasVigentes = () => {
    return JSON.stringify(modelo.tareas.listarTareasVigentes());
}

let listarTareasVigentesPorPrioridad = (prioridad) => {
    return JSON.stringify(modelo.tareas.listarTareasVigentesPorPrioridad(prioridad));
}

let nuevaTarea = (tarea) => {
    let resultado = {};
    if (modelo.tareas.nuevaTarea(tarea.id,
        tarea.texto, tarea.fechaTope, tarea.prioridad)) {
        resultado.error = false;
        resultado.mensajeError = "";
    } else {
        resultado.error = true;
        resultado.mensajeError = "Error al añadir la nueva tarea";
    }
    return JSON.stringify(resultado);
}

let modificarTarea = (tarea) => {
    let resultado = {};
    if (modelo.tareas.modificarTarea(tarea.id,
        tarea.texto, tarea.fechaTope, tarea.prioridad)) {
        resultado.error = false;
        resultado.mensajeError = "";
    } else {
        resultado.error = true;
        resultado.mensajeError = "Error al modificar la tarea";
    }
    return JSON.stringify(resultado);
}

let borrarTarea = (id) => {
    let resultado = {};
    if ((modelo.tareas.buscarTareaPorId(id) === false)) {
        resultado.error = true;
        resultado.mensajeError = "La tarea con ese id no existe";
    }
    else if (modelo.tareas.borrarTarea(id)) {
        resultado.error = false;
        resultado.mensajeError = "";
    }
    else {
        resultado.error = true;
        resultado.mensajeError = "Error al eliminar la tarea";
    }
    return JSON.stringify(resultado);
}

let atenderPeticion = (request, response) => {
    response.writeHead(200, { "Content-Type": "text/plain; charset=utf-8" });
    if (request.method === 'GET' && request.url === '/tareas') {
            response.write(listarTareas());
            response.end();
    } 
    else if (request.method === 'GET' && request.url.startsWith("/tarea/")) {
        let partes = request.url.split('/');
        let id = partes[2];
        response.write(fichaTarea(id));
        response.end();
    }
    else if (request.method === 'GET' && request.url === ("/tareas_vigentes")) {
        response.write(listarTareasVigentes());
        response.end();
    } 
    else if (request.method === 'GET' && request.url.startsWith("/tareas_vigentes/")) {
        let partes = request.url.split('/');
        let prioridad = partes[2];
        response.write(listarTareasVigentesPorPrioridad(prioridad));
        response.end();
    } 
    else if (request.method === 'POST' && request.url === '/tarea') {
        let body = [];
        request.on('data', (chunk) => {
            body.push(chunk);
        }).on('end', () => {
            body = Buffer.concat(body).toString();
            response.write(nuevaTarea(JSON.parse(body)));
            response.end();
        });
    } 
    else if (request.method === 'PUT' && request.url === '/tarea') {
        let body = [];
        request.on('data', (chunk) => {
            body.push(chunk);
        }).on('end', () => {
            body = Buffer.concat(body).toString();
            response.write(modificarTarea(JSON.parse(body)));
            response.end();
        });
    } 
    else if (request.method === 'DELETE') {
        let partes = request.url.split('/');
        let id = partes[2];
        response.write(borrarTarea(id));
        response.end();
    }  
}

http.createServer(atenderPeticion).listen(8080);