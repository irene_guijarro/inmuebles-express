const fs = require('fs');
const moment = require('moment');
const archivo = 'tareas.json';

let cargarTareas = () => {
    try {
        return JSON.parse(fs.readFileSync(archivo, 'utf8'));
    } catch (e) {
        return [];
    }
};

let guardarTareas = (tareas) => {
    fs.writeFileSync(archivo, JSON.stringify(tareas));
};

let buscarTareaPorId =  (id) => {
    let tareas = cargarTareas();
    let resultado = tareas.filter((tarea) => tarea.id == id);
    if (resultado.length > 0)
        return resultado[0];
    else
        return false;
};

let esValidaNuevaTarea = (id, texto, fecha, prioridad) => {
    let nfecha = new moment(fecha,  "DD/MM/YYYY hh:mm");
    let contInvalidos = 0;

    if (id === null || texto === "" || nfecha === "" || prioridad === "")
        contInvalidos++;
    if (buscarTareaPorId(id))
        contInvalidos++;
    if (nfecha.fromNow().includes("ago"))
        contInvalidos++;
    if (prioridad !== "ALTA" && prioridad !== "MEDIA" && prioridad !== "BAJA")
        contInvalidos++;
    
    if (contInvalidos > 0)
        return false;
    else
        return true;
};

let nuevaTarea = (id, texto, fecha, prioridad) => {
    if (esValidaNuevaTarea(id, texto, fecha, prioridad)) {
        let tareas = cargarTareas();
        let nuevo = {
            id: id,
            texto: texto,
            fechaTope: fecha,
            prioridad: prioridad
        };
        tareas.push(nuevo);
        guardarTareas(tareas);
        return true; // Se ha añadido la nueva tarea
    }
    return false; // No se ha añadido la nueva tarea
};

let borrarTarea = (id) => {
    let tareas = cargarTareas();
    let tareasFiltradas = tareas.filter((tarea) => tarea.id != id);
    if (tareasFiltradas.length != tareas.length) {
        guardarTareas(tareasFiltradas);
        return true; // Se ha eliminado la tarea
    }
    return false; // No se ha eliminado la tarea
};

let modificarTarea = (id, texto, fecha, prioridad) => {
    if (buscarTareaPorId(id)) {
        if (borrarTarea(id)) {
            if (nuevaTarea(id, texto, fecha, prioridad))
                return true; // Se ha podido modificar
            else
                return false; // No se ha podido modificar
        }
    }

};

let listarTareasVigentes = () => {
    let tareas = cargarTareas();
    let resultado = tareas.filter(tarea => {
        let fecha = new moment(tarea.fechaTope,  "DD/MM/YYYY hh:mm");
        return fecha.fromNow().includes("in");
    });
    if (resultado.length > 0)
        return resultado;
    else    
        return [];
}

let listarTareasVigentesPorPrioridad = (prioridad) => {
    let tareasVigentes = listarTareasVigentes();
    let resultado = tareasVigentes.filter(tarea => 
        tarea.prioridad === prioridad);
    if (resultado.length > 0) 
        return resultado;
    else
        return [];
}

module.exports = {
    listarTareas: cargarTareas,
    nuevaTarea: nuevaTarea,
    buscarTareaPorId: buscarTareaPorId,
    borrarTarea: borrarTarea,
    modificarTarea: modificarTarea,
    listarTareasVigentes: listarTareasVigentes,
    listarTareasVigentesPorPrioridad: listarTareasVigentesPorPrioridad
};