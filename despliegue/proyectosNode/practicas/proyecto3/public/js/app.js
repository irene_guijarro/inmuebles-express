function eliminarInmueble(id)
{
    $.ajax({
        url:"/inmuebles/" + id,
        type:"DELETE",
        success: function(data) {
            if (data.ok) {
                window.location.assign("http://localhost:8080/inmuebles");
            }   
        }
    });
}