const express = require('express');
const index = require('./routes/index');
const inmuebles = require('./routes/inmuebles');
const tipos = require('./routes/tipos');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const fileUpload = require('express-fileupload');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/inmuebles');

let app = express();

app.use(fileUpload());
app.set('view engine', 'ejs');
app.use('/public', express.static('./public'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use('/', index)
app.use('/inmuebles', inmuebles);
app.use('/tipos', tipos);

app.listen(8080);