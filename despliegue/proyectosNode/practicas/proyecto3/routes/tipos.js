const express = require('express');
const Tipo = require('../models/tipo');

let router = express.Router();

router.get('/', (req, res) => {
    Tipo.find().then(resultado => {
        res.send(resultado);
    })
});

module.exports = router;