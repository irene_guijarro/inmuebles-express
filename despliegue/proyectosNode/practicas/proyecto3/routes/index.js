const express = require('express');

let router = express.Router();

router.get('/', (req, res) => {
    res.render('index');
});

router.get('/nuevo_inmueble', (req, res) => {
    res.render('nuevo_inmueble');
    
})

module.exports = router;