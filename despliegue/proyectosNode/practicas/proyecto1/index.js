const http = require('http');
const https = require('https');
const jwt = require('jsonwebtoken');
const Event = require('./modelo/event');
const User = require('./modelo/user');
const fs = require('fs');

let guardarImagen = (url, name) => {
    https.request(url)
    .on('response', function(res) {
        let body = '';
        res.setEncoding('binary');
        res.on('data', function(chunk) {
            body += chunk;
        }).on('end', function() {
            let fileName = "img/" + name + ".png";
            fs.writeFileSync(fileName, body, 'binary');
        });
    })
    .end();
}

let cuerpoImagen = url => {
    https.request(url)
    .on('response', function(res) {
        let body = '';
        res.setEncoding('binary');
        res.on('data', function(chunk) {
            body += chunk;
        }).on('end', function() {
            return body;
        });
    })
    .end();
}
    

let atenderPeticion = (request, response) => {
    // Autenticacion y registro (I)
    if (request.method === 'POST' && request.url === '/auth/register') {
        response.writeHead(200, {"Content-Type":"application/json;"});
        let body = [];
        request.on('data', (chunk) => {
            body.push(chunk);
        }).on('end', () => {
            body = Buffer.concat(body).toString();
            let usuarioJSON = JSON.parse(body);
            let respuesta;
            User.registrarUsuario(usuarioJSON).then((res) => {
                if (!res.error) {
                    respuesta = {
                        error: false,
                        result: {id: res}
                    };
                }
                else {
                    respuesta = {
                        error: true,
                        mensajeError: "Error in user register"
                    };
                }
                response.write(JSON.stringify(respuesta));
                response.end();
            }).catch((error) => { 
                respuesta = {error:true, mensajeError:"Error registering user"}; 
                response.write(JSON.stringify(respuesta));
                response.end();
            });
        });
    } else if (request.method === 'POST' && request.url === '/auth/login') {
        response.writeHead(200, { "Content-Type":"application/json;" });
        let body = [];
        request.on('data', (chunk) => {
            body.push(chunk);
        }).on('end', () => {
            body = Buffer.concat(body).toString();
            let usuarioJSON = JSON.parse(body);
            let respuesta;
            User.validarUsuario(usuarioJSON).then((res) => {
                if (!res.error) {
                    respuesta = {
                        error:res.error,
                        token:res.token
                    };
                }
                else {
                    respuesta = {
                        error:res.error,
                        mensajeError:res.mensajeError
                    };
                }
                response.write(JSON.stringify(respuesta));
                response.end();
            }).catch((error) => { 
                respuesta = {error:true, mensajeError:"Ha habido un error"}; 
                response.write(JSON.stringify(respuesta));
                response.end();
            });
             
        });
    } else if (request.method === 'GET' && request.url === '/auth/token') {
        response.writeHead(200, { "Content-Type":"application/json;" });
        let token = request.headers['authorization'];
        let respuesta = User.validarToken(token);
        if (respuesta === "Token no valido") {
            respuesta = {
                error: true
            }
        } else {
            respuesta = {
                error: false
            }
        }
        response.write(JSON.stringify(respuesta));
        response.end();
    }

    // Autenticacion y registro (II)
    else if (request.method === 'GET' && request.url === '/auth/google') {
        let token = request.headers['authorization'];
        let respuesta;
        let imagen;
        let usuarioJSON;
        https.request('https://www.googleapis.com/plus/v1/people/me?access_token=' + token)
        .on('response', res => {
            let body = '';
            res.on('data', chunk => {
                body += chunk;
            }).on('end', function() { 
                let datos = JSON.parse(body);
                imagen = cuerpoImagen(datos.image.url);
                guardarImagen(datos.image.url, datos.displayName);
                usuarioJSON = {
                    id: datos.id,
                    name: datos.displayName,
                    email: datos.emails[0].value,
                    image: datos.image.url
                };
                User.registrarUsuarioGoogle(usuarioJSON).then((res) => {
                    if (res) {
                        respuesta = {
                            error: res
                        }
                    }
                    response.write(JSON.stringify(respuesta));
                    response.end();
                })
            });
        })
        .end();
        response.write(JSON.stringify(respuesta));
        response.end();
    } else if (request.url === '/auth/facebook' && request.method === 'GET') {
        let token = request.headers['authorization'];
        let respuesta;
        https.request('https://graph.facebook.com/v2.11/me?fields=id,name,email,picture&access_token=' + token)
        .on('response', function(res) {
            let body = '';
            res.on('data', function(chunk) {
                body += chunk
            })
            .on('end', function() {
                let datos = JSON.parse(body);
                respuesta = {
                    id: datos.id,
                    name: datos.name,
                    email: datos.email,
                    picture: datos.picture.data.url
                }
                response.write(JSON.stringify(respuesta));
                response.end();
            });
        })
        .end();
    }
}

http.createServer(atenderPeticion).listen(8080);