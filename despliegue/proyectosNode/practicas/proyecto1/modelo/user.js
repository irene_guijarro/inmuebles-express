const conexion = require('./bdconfig');
const jwt = require('jsonwebtoken');
const md5 = require('md5');
const secreto = 'daw';

module.exports = class Usuario {

    static registrarUsuario(usuarioJSON) {
        let passEncriptada = md5(usuarioJSON.password);
        let datos = {name: usuarioJSON.name, email: usuarioJSON.email, password: passEncriptada, avatar: usuarioJSON.avatar, lat: usuarioJSON.lat, lng: usuarioJSON.lng};
        return new Promise( (resolve, reject) => {
            conexion.query("INSERT INTO user SET ?", datos,
            (error, resultado, campos) => {
                if (error)
                    return reject(error);
                else
                    resolve(resultado.insertId);
            });
        }); 
    }

    static registrarUsuarioGoogle(usuarioJSON) {
        let datos = {id_google: usuarioJSON.id, name: usuarioJSON.name, email: usuarioJSON.email, avatar: usuarioJSON.image, lat: 0, lng: 0};
        let querySelect = "SELECT * FROM user WHERE id_google = '" + datos.id_google + "';";
        return new Promise((resolve, reject) => {
            conexion.query(querySelect, (error, resultado, campos) => {
                if (resultado === "") {
                    return resolve(true)
                    // hacer el insert
                    // generar token y devolverlo
                }
                else {
                    return resolve(false);
                    // generar token y devolverlo
                }
            })
        })
    }
    
    static validarUsuario(usuarioJSON) {
        let passEncriptada = md5(usuarioJSON.password);
        let query = "SELECT * FROM user WHERE email = '" + usuarioJSON.login + "' AND password = '" + passEncriptada + "';";
        return new Promise( (resolve, reject) => {
            conexion.query(query,
                (error, resultado, campos) => {
                    if (error) {
                        return reject(error);
                    }
                        
                    else {
                        if (resultado.length == 1) {
                            let respuesta = {
                                error:false,
                                token:this.generarToken(usuarioJSON.login)
                            };
                            resolve(respuesta);
                        }
                        else {
                            let respuesta = {
                                error:true,
                                mensajeError:"El usuario o la password son incorrectos"
                            };
                            resolve(respuesta);
                        }
                    }
                });
        });
    }

    static generarToken(login) {
        let token = jwt.sign({login: login}, secreto, {expiresIn:"30 minutes"});
        return token;
    }

    static validarToken(token) {
        try {
            let resultado = jwt.verify(token, secreto);
            return resultado;
        } catch (e) {
            return "Token no valido";
        }
    }
}