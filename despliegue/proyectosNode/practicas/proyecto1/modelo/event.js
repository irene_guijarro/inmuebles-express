const conexion = require('./bdconfig');

module.exports = class Event
{
    constructor(eventJSON) {
        this.creator = eventJSON.creator;
        this.title = eventJSON.title;
        this.description = eventJSON.description;
        this.date = eventJSON.date;
        this.price = eventJSON.price;
        this.lat = eventJSON.lat;
        this.lng = eventJSON.lng;
        this.address = eventJSON.address;
        this.image = eventJSON.image;
    }

    static listarEventos() {
        return new Promise ( (resolve, reject) => {
            conexion.query("SELECT * FROM events;", 
                (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else
                        resolve(resultado.map(tJSON => new Tarea(tJSON)));
                });
        });
    }
}