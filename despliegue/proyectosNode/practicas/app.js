﻿const express = require('express');
const auth = require('./routes/auth');
const events = require('./routes/events');
const users = require('./routes/users');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');

let app = express();

app.use(cors());
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: true }, {limit: '50mb'}));
app.use('/auth', auth);
app.use('/events', events);
app.use('/users', users);
app.use('/img', express.static('./public'));


module.exports.app = app;
