const http = require('http');
const jwt = require('jsonwebtoken');
const Tarea = require('./modelo/tarea');
const Usuario = require('./modelo/usuario');

let validarToken = (token) => {
    let login = Usuario.validarToken(token).login;
    let tokenRenovado = Usuario.generarToken(login);
    return tokenRenovado;
}

let atenderPeticion = (request, response) => {
    if (request.method === 'POST' && request.url === '/login') {
        response.writeHead(200, { "Content-Type":"application/json;" });
        let body = [];
        request.on('data', (chunk) => {
            body.push(chunk);
        }).on('end', () => {
            body = Buffer.concat(body).toString();
            let usuarioJSON = JSON.parse(body);
            let respuesta;
            Usuario.validarUsuario(usuarioJSON).then((res) => {
                if (!res.error) {
                    respuesta = {
                        error:res.error,
                        token:res.token
                    };
                }
                else {
                    respuesta = {
                        error:res.error,
                        mensajeError:res.mensajeError
                    };
                }
                response.write(JSON.stringify(respuesta));
                response.end();
            }).catch((error) => { 
                respuesta = {error:true, mensajeError:"Ha habido un error"}; 
                response.write(JSON.stringify(respuesta));
                response.end();
            });
             
        });
    } else if (request.method === 'GET' && request.url === '/tareas') {
        response.writeHead(200, { "Content-Type":"application/json;" });
        let respuesta;
        let token = request.headers['authorization'];
        let login = Usuario.validarToken(token).login;
        let tokenRenovado = Usuario.generarToken(login);

        Tarea.listarTareas(login).then((res) => {
            respuesta = {
                tareas:res,
                tokenRenovado:tokenRenovado
            };
            response.write(JSON.stringify(respuesta));
            response.end();
        }).catch((error) => {
            respuesta = {
                error:true,
                mensajeError:"Ha habido un error mostrando las tareas."
            };
            response.write(JSON.stringify(respuesta));
            response.end();
        });
    } else if (request.method === 'GET' && request.url.startsWith('/tarea/')) {
        let partes = request.url.split('/');
        let id = partes[2];
        let token = request.headers['authorization'];
        let login = Usuario.validarToken(token).login;
        let tokenRenovado = Usuario.generarToken(login);

        Tarea.buscarTareaPorId(id, login).then((res) => {
            respuesta = {
                token: tokenRenovado,
                error: false,
                mensajeError: "",
                tarea:res
            };
            response.write(JSON.stringify(respuesta));
            response.end();
        }).catch((error) => {
            respuesta = {
                error: true,
                mensajeError: "Error mostrando la tarea: " + error 
            };
            response.write(JSON.stringify(respuesta));
            response.end();
        });
    } else if (request.method === 'GET' && request.url === ('/tareas_vigentes')) {
        let token = request.headers['authorization'];
        let login = Usuario.validarToken(token).login;
        let tokenRenovado = Usuario.generarToken(login);

        Tarea.listarTareasVigentes(login).then((res) => {
            respuesta = {
                error: false,
                token: tokenRenovado,
                tareasVigentes: res
            };
            response.write(JSON.stringify(respuesta));
            response.end();
        }).catch((error) => {
            respuesta = {
                error: true,
                mensajeError: "Error mostrando tareas vigentes"
            };
            response.write(JSON.stringify(respuesta));
            response.end();
        })
    } else if (request.method === 'GET' && request.url.startsWith('/tareas_vigentes/')) {
        let partes = request.url.split('/');
        let prioridad = partes[2]; 
        let token = request.headers['authorization'];
        let login = Usuario.validarToken(token).login;
        let tokenRenovado = Usuario.generarToken(login);

        Tarea.listarTareasVigentesPorPrioridad(login, prioridad).then((res) => {
            respuesta = {
                error: false,
                token: tokenRenovado,
                tareasVigentes: res
            };
            response.write(JSON.stringify(respuesta));
            response.end();
        }).catch((error) => {
            respuesta = {
                error: true,
                mensajeError: "Error mostrando tareas vigentes de prioridad " + prioridad
            };
            response.write(JSON.stringify(respuesta));
            response.end();
        })
    } else if (request.method === 'POST' && request.url === ('/tarea')) {
        let token = request.headers['authorization'];
        let login = Usuario.validarToken(token).login;
        let tokenRenovado = Usuario.generarToken(login);

        let body = [];
        request.on('data', (chunk) => {
            body.push(chunk);
        }).on('end', () => {
            body = Buffer.concat(body).toString();
            let nuevaTareaJSON = JSON.parse(body);
            let nuevaTarea = new Tarea(nuevaTareaJSON);
            nuevaTarea.usuario = login;
            nuevaTarea.crear().then((res) => {
                respuesta = {
                    token: tokenRenovado,
                    error: false,
                    mensajeError: "",
                    nuevoId: res.id
                };
                response.write(JSON.stringify(respuesta));
                response.end();
            }).catch((error) => {
                respuesta = {
                    error: true,
                    mensajeError: "Error al crear la tarea nueva"
                };
                response.write(JSON.stringify(respuesta));
                response.end();
            });
        }); 
    } else if (request.method === 'PUT' && request.url === ('/tarea')) {
        let token = request.headers['authorization'];
        let login = Usuario.validarToken(token).login;
        let tokenRenovado = Usuario.generarToken(login);

        let body = [];
        request.on('data', (chunk) => {
            body.push(chunk);
        }).on('end', () => {
            body = Buffer.concat(body).toString();
            let nuevaTareaJSON = JSON.parse(body);
            let nuevaTarea = new Tarea(nuevaTareaJSON);
            nuevaTarea.usuario = login;
            nuevaTarea.actualizar().then((res) => {
                respuesta = {
                    token: tokenRenovado,
                    error: false,
                    mensajeError: "",
                    filasAfectadas: res
                };
                response.write(JSON.stringify(respuesta));
                response.end();
            }).catch((error) => {
                respuesta = {
                    error: true,
                    mensajeError: "Error al actualizar la tarea"
                };
                response.write(JSON.stringify(respuesta));
                response.end();
            });
        });
    } else if (request.method === 'DELETE' && request.url.startsWith('/tarea/')) {
        let partes = request.url.split('/');
        let id = partes[2];

        let token = request.headers['authorization'];
        let login = Usuario.validarToken(token).login;
        let tokenRenovado = Usuario.generarToken(login);

        let tareaBorrar = new Tarea({id: id, texto: "", fechaTope: "2017-03-23", prioridad: "ALTA", usuario: login});
        tareaBorrar.borrar().then((res) => {
            respuesta = {
                error: false,
                mensajeError: "",
                token: tokenRenovado,
                filasAfectadas: res
            };
            response.write(JSON.stringify(respuesta));
            response.end();
        }).catch((error) => {
            respuesta = {
                error: true,
                mensajeError: "Error al borrar la tarea"
            };
            response.write(JSON.stringify(respuesta));
            response.end();
        });
        
    }
}

http.createServer(atenderPeticion).listen(8080);