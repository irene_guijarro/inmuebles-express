const conexion = require('./bdconfig');
const jwt = require('jsonwebtoken');
const md5 = require('md5');
const secreto = 'rubeola';

module.exports = class Usuario {
    
    static validarUsuario(usuarioJSON) {
        let passEncriptada = md5(usuarioJSON.password);
        let query = "SELECT * FROM usuarios WHERE (login = '" + usuarioJSON.login + "') AND (password = '" + passEncriptada + "');";
        return new Promise( (resolve, reject) => {
            conexion.query(query,
                (error, resultado, campos) => {
                    console.log(resultado);
                    if (error) {
                        return reject(error);
                    }
                        
                    else {
                        if (resultado.length == 1) {
                            let respuesta = {
                                error:false,
                                token:this.generarToken(usuarioJSON.login)
                            };
                            resolve(respuesta);
                        }
                        else {
                            let respuesta = {
                                error:true,
                                mensajeError:"El usuario o la password son incorrectos"
                            };
                            resolve(respuesta);
                        }
                    }
                });
        });
    }

    static generarToken(login) {
        let token = jwt.sign({login: login}, secreto, {expiresIn:"30 minutes"});
        return token;
    }

    static validarToken(token) {
        try {
            let resultado = jwt.verify(token, secreto);
            return resultado;
        } catch (e) {
            return "Token no valido";
        }
    }
}