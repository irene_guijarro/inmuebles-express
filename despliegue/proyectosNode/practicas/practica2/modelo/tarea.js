const conexion = require('./bdconfig');

module.exports = class Tarea
{
    constructor(tareaJSON) {
        this.id = tareaJSON.id;
        this.texto = tareaJSON.texto;
        this.fechaTope = tareaJSON.fechaTope;
        this.prioridad = tareaJSON.prioridad;
        this.usuario = tareaJSON.usuario;
    }

    static listarTareas(usuario) {
        return new Promise ( (resolve, reject) => {
            conexion.query("SELECT * FROM tareas WHERE usuario='" + usuario + "';", 
                (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else
                        resolve(resultado.map(tJSON => new Tarea(tJSON)))
                });
        });
    }

    static buscarTareaPorId(id, usuario) {
        let query = "SELECT * FROM tareas WHERE (id=" + id + ") AND (usuario='" + usuario + "');";
        return new Promise ( (resolve, reject) => {
            conexion.query(query, (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else {
                        resolve(resultado.map(tJSON => new Tarea(tJSON)));
                    }
                });
        });
    }

    crear() {
        return new Promise( (resolve, reject) => {
            let datos = {texto: this.texto, fechaTope: this.fechaTope, prioridad: this.prioridad, usuario: this.usuario};
            conexion.query("INSERT INTO tareas SET ?", datos,
                (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else
                        resolve(resultado);
                });
        });
    }

    borrar() {
        return new Promise((resolve, reject) => {
            conexion.query("DELETE FROM tareas WHERE id=" + this.id,
                (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else
                        resolve(resultado.affectedRows);
                });
        });
    }

    actualizar() {
        return new Promise( (resolve, reject) => {
            let datos = {texto: this.texto, fechaTope: this.fechaTope, prioridad: this.prioridad, usuario: this.usuario};
            conexion.query("UPDATE tareas SET ? WHERE id=" + this.id, datos,
                (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else {
                        resolve(resultado.affectedRows);
                    }
                });            
        });
    }

    static listarTareasVigentes(usuario) {
        let query = "SELECT * FROM tareas WHERE (usuario='" + usuario + "') AND (fechaTope > NOW());";
        return new Promise((resolve, reject) => {
            conexion.query(query,
                (error, resultado, campos) => {
                    console.log(resultado);
                    if (error)
                        return reject(error);
                    else
                        resolve(resultado.map((tareaJSON) => new Tarea(tareaJSON)));
                });
        })
    }

    static listarTareasVigentesPorPrioridad(usuario, prioridad) {
        return new Promise((resolve, reject) => {
            conexion.query("SELECT * FROM tareas WHERE (usuario='" + usuario + "') AND (fechaTope > NOW()) AND (prioridad='" + prioridad + "');",
                (error, resultado, campos) => {
                    if (error)
                        return reject(error);
                    else
                        resolve(resultado.map((tareaJSON) => new Tarea(tareaJSON)));
                });
        })
    }
}