<?php

abstract class SelectorIndividual implements ISelectorIndividual
{
    protected $titulo;
    protected $nombre;
    protected $contenido = [];
    protected $elemSeleccionado;

    public function __construct($titulo, $nombre, $contenido, $elemSeleccionado)
    {
        $this->titulo = $titulo;
        $this->nombre = $nombre;
        $this->contenido = $contenido;
        $this->elemSeleccionado = $elemSeleccionado;
    }

    abstract function generaSelector();
}