<?php

class SISelect extends SelectorIndividual
{
    public function __construct($titulo, $nombre, $contenido, $elemSeleccionado)
    {
        parent::__construct($titulo, $nombre, $contenido, $elemSeleccionado);
    }

    public function generaSelector()
    {
        echo '<label for="' . $this->nombre . '">' . $this->titulo. '</label>';
        echo "<select name=\"$this->nombre\" id=\"$this->nombre\">";

        $i = 0;
        foreach ($this->contenido as $clave => $valor)
        {
            if ($i === $this->elemSeleccionado) {
                echo "<option value=\"$clave\" selected>$valor</option>";
            }
            else
                echo "<option value=\"$clave\">$valor</option>";
            $i ++;
        }
        echo "</select>";
        echo "<br>";
    }
}