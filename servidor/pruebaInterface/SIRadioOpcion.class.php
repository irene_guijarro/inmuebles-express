<?php

class SIRadioOpcion extends SelectorIndividual
{

    public function __construct($titulo, $nombre, $contenido, $elemSeleccionado)
    {
        parent::__construct($titulo, $nombre, $contenido, $elemSeleccionado);
    }

    public function generaSelector()
    {
        echo '<label for="' . $this->nombre . '">' . $this->titulo. '</label>';

        $i = 0;
        foreach($this->contenido as $clave => $valor)
        {
            if ($i === $this->elemSeleccionado) {
                echo "<input type=\"radio\" name=\"$this->nombre\" value=\"$valor\" id=\"$valor\" checked>$clave";
            }
            else
                echo "<input type=\"radio\" name=\"$this->nombre\" value=\"$valor\" id=\"$valor\">$clave";
            $i ++;
        }
        echo "<br>";
    }
}