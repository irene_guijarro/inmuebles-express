<?php
require 'ISelectorIndividual.interface.php';
require 'SelectorIndividual.class.php';
require 'SIRadioOpcion.class.php';
require 'SISelect.class.php';

$contenido = array (
    'hello' => 'hola',
    'goodbye' => 'adios',
    'cat' => 'gato',
    'dog' => 'perro'
);

$select = new SISelect('Traducir español-inglés', 'traductorEI', $contenido, 2);
$radio = new SIRadioOpcion('Traducir inglés-español', 'traductorIE', $contenido, 1);
?>
    <form id="form" method="post" action="<?= $_SERVER['PHP_SELF']?>">
<?php
$select->generaSelector();
$radio->generaSelector();
?>
    <input type="submit" name="enviar" value="Traducir">
</form>

<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $postSelect = $_POST['traductorEI'];
    $postRadio = $_POST['traductorIE'];
    echo "Significa $postSelect en traductor español-inglés";
    echo "<br>";
    echo "Significa $postRadio en traductor inglés-español";
}
