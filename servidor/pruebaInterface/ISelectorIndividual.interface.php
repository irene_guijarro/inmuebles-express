<?php
interface ISelectorIndividual
{
    public function __construct($titulo, $nombre, $contenido, $elemSeleccionado);

    public function generaSelector();
}