<?php

class Grupo
{
    private $id;
    private $nombre;
    private $num_contactos;

    public function __construct($id, $nombre, $num_contactos)
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->num_contactos = $num_contactos;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getNumContactos()
    {
        return $this->num_contactos;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function setNumContactos($num_contactos)
    {
        $this->num_contactos = $num_contactos;
    }
}