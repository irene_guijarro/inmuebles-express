<?php

class EContacto extends Contacto
{
    private $email;
    private $url;

    public function __construct($id, $nombre, $telefono, $foto, $email, $url)
    {
        parent::__construct($id, $nombre, $telefono, $foto);
        $this->email = $email;
        $this->url = $url;
    }

    public function __toString()
    {
        $strPadre = parent::__toString();
        return "$strPadre $this->email $this->url";
    }
}