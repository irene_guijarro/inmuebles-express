<?php

class PContacto extends Contacto
{
    private $direccion;
    private $ciudad;
    private $provincia;

    public function __construct($id, $nombre, $telefono, $foto, $direccion, $ciudad, $provincia)
    {
        parent::__construct($id, $nombre, $telefono, $foto);
        $this->direccion = $direccion;
        $this->ciudad = $ciudad;
        $this->provincia = $provincia;
    }

    public function __toString()
    {
        $strPadre = parent::__toString();
        return "$strPadre $this->direccion $this->ciudad $this->provincia";
    }
}