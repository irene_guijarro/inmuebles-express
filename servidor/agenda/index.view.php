<?php
include 'partials/header.part.php';
?>
<form action="<?= $_SERVER['PHP_SELF']; ?>"
      method="post" enctype="multipart/form-data">
    <div>
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" id="nombre">
    </div>
    <div>
        <label for="telefono">Teléfono</label>
        <input type="text" name="telefono" id="telefono">
    </div>
    <div>
        <label for="fecha">Fecha</label>
        <input type="text" name="fecha" id="fecha">
    </div>
    <div>
        <label for="foto">foto</label>
        <input type="file" name="foto" id="foto" multiple>
    </div>
    <div>
        <select>
            <?php foreach ($grupos as $grupo) : ?>
                <option value="<?= $grupo->getId(); ?>"><?= $grupo->getId(); ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <input type="submit" name="enviar" value="Enviar">
</form>

<div id="error">
    <?php if (isset($error) === true) : ?>
        <?= $error ?>
    <?php endif;?>
</div>

<div>
    <form id="search" method="post" action="<?= $_SERVER['PHP_SELF'] ?>">
        <label for="buscar">Buscar</label>
        <input type="text" name="buscar">
        <input type="submit" name="btBuscar" value="buscar">
    </form>
</div>
<table>
    <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>Teléfono</th>
        <th>Fecha alta</th>
        <th>Foto</th>
        <th>Operaciones</th>
    </tr>
    <?php foreach($contactos as $contacto) : ?>
        <tr>
            <td><?= $contacto->getId(); ?></td>
            <td><?= $contacto->getNombre(); ?></td>
            <td><?= $contacto->getTelefono(); ?></td>
            <td><?= $contacto->getFechaAlta(); ?></td>
            <td><img src="<?= $contacto->getFoto(); ?>" alt="Foto contacto" width="100"></td>
            <td><a href="<?= $_SERVER['PHP_SELF'] . '?operacion=borrar&id=' . $contacto->getId() ?>">Eliminar</a></td>
        </tr>
    <?php endforeach; ?>
</table>

<?php
include 'partials/footer.part.php';
?>
