<?php
    include 'header.part.php';
?>

    <h1>ALUMNOS</h1>

    <table>
        <tr>
            <th>NOMBRE</th><th>edad</th>
            <?php foreach ($alumnos as $alumno) : ?>
                <tr class="<?= $alumno['edad'] > 18 ? 'rojo' : 'azul'; ?>">
                    <td><?= $alumno['nombre']; ?></td>
                    <td><?= $alumno['edad']; ?></td>
                </tr>
            <?php endforeach; ?>
        </tr>
    </table>

<?php
    include 'footer.part.php';
?>
