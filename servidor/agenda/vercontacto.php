<?php
require 'Agenda.class.php';
require 'Contacto.class.php';
require 'EContacto.class.php';
require 'PContacto.class.php';

$agenda = new Agenda();

$contacto1 = new Contacto( 1, 'Irene', '664282914', 'url de la foto');
$agenda->addContacto($contacto1);

$contacto2 = new Contacto( 2, 'Pepe', '664282914', 'url de la foto');
$agenda->addContacto($contacto2);

$contacto3 = new Contacto( 3, 'Jose', '664282914', 'url de la foto');
$agenda->addContacto($contacto3);

$pcontacto1 = new PContacto(4, 'Celia', '778899654', 'url foto',
    'Calle falsa 123', 'Springfield', 'Kansas');
$agenda->addContacto($pcontacto1);

$pcontacto2 = new PContacto(5, 'Juan Guijarro', '8887765', 'blbl',
    'Almassera 25','San Vicente', 'Alicante');
$agenda->addContacto($pcontacto2);

$econtacto1 = new EContacto(6, 'Loli', '664537198', 'foto',
    'madomamija@gmail.com', 'loli.com');
$agenda->addContacto($econtacto1);

$econtacto2 = new EContacto(7, 'Tempi', '664537198', 'foto',
    'tempiguapa@gmail.com', 'tempi.es');
$agenda->addContacto($econtacto2);

$id = $_GET['id'] ?? null;

if(!is_null($id))
{
    echo '<p>Contacto</p>';

    $contacto = $agenda->findById($id);

    if ($contacto !== false)
        echo $contacto;
    else
        echo "<p>No se ha encontrado el contacto con id $id</p>";
}
else
    echo "<p>No se ha recibido el id del contacto</p>";