<?php

class Agenda
{
    private $contactos = [];

    public function getContacto(int $pos) {
        return $this->contactos[$pos];
    }

    public function addContacto(Contacto $contacto) {
        $this->contactos[] = $contacto;
    }

    public function findById($id) {
        $resultado = array_filter($this->contactos, function($contacto) use ($id) {
            $idContacto = $contacto->getId();
            return $idContacto == $id; // Utilizo el == porque el $id que se recoge por parámetro viene del _GET, por tanto es un string
        });

        if (count($resultado) === 0)
            return false; //No ha encontrado coincidencias
        return current($resultado); //current devuelve el valor del elemento del array que está siendo apuntado por el puntero interno
    }

    public function removeContacto(Contacto $contacto_a_eliminar) {
        $pos = array_search($contacto_a_eliminar, $this->contactos, true);

        if ($pos !== false) {
            unset($this->contactos[$pos]);
        }

        $this->contactos = array_values($this->contactos);
    }

    public function __toString()
    {
        $strAgenda = "<p>Agenda</p>";

        foreach ($this->contactos as $cont)
            $strAgenda .= $cont . '<a href="vercontacto.php?id=' . $cont->getId() . '">Ver Contacto</a><br>';

        return $strAgenda;
    }
}