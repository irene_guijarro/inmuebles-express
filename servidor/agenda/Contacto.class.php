<?php
class Contacto {
    private $id;
    private $nombre;
    private $telefono;
    private $fecha_alta;
    private $foto;

    public function __construct($id, $nombre, $telefono, $foto)
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->telefono = $telefono;
        $this->fecha_alta = new DateTime();
        $this->foto = $foto;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    public function getTelefono()
    {
        return $this->telefono;
    }

    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
        return $this;
    }

    public function getFechaAlta()
    {
        return $this->fecha_alta;
    }

    public function setFechaAlta($fecha_alta)
    {
        $this->fecha_alta = $fecha_alta;
        return $this;
    }

    public function getFoto()
    {
        return $this->foto;
    }

    public function setFoto($foto)
    {
        $this->foto = $foto;
        return $this;
    }

    public function __get($id)
    {
        return $this->id;
    }

    public function __toString()
    {
        $strFecha = $this->fecha_alta->format('d/m/Y H:i:s');
        return "$this->id $this->nombre $this->telefono $strFecha";
    }
}