<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Fechas</title>
</head>
<body>
<h1>
    <?php
        // Muestra la fecha y horas actuales formato dd/mm/yyyy hh:mm:ss
        $fecha = new DateTime();
        $fecha = date("d/m/Y G:i:s");
        echo $fecha;
        echo "<br>";

        // Muestra el nombre de la zona horaria que se utiliza por defecto
        echo date_default_timezone_get();
        echo "<br>";

        // Fecha dentro de 45 días
        // Forma 1
        $fecha2 = new DateTime(date("Y/m/d"));
        $fecha2 -> add(new DateInterval('P45D'));
        echo $fecha2 -> format('d/m/Y');
        echo "<br>";

        // Forma 2, ¿Menos correcta?
        echo date("d/m/Y", strtotime("+45 day"));
        echo "<br>";

        //  Muestra el número de días que han pasado desde el 1 de enero.
        $fechaHoy = new DateTime();
        $fechaInicio  = new DateTime('2017-01-01');
        $diferencia = date_diff($fechaInicio, $fechaHoy);
        echo "Días de diferencia: " . $diferencia ->days;
        echo "<br>";

        // Muestra la fecha y hora actuales de Nueva York.
        $d = new DateTime("now", new DateTimeZone("America/New_York"));
        echo $d -> format("d/m/Y G:i:s");
        echo "<br>";

        //  Muestra el día de la semana que era el 1 de enero de este año.
        $day = strftime("%A",strtotime("2017-01-01"));
        echo $day;
    ?>
</h1>
</body>
</html>