<?php
$alumnos = [
    [
        "id" => 1,
        "nombre" => "Irene",
        "edad" => 18
    ],
    [
        "id" => 2,
        "nombre" => "Juanjo",
        "edad" => 22
    ],
    [
        "id" => 3,
        "nombre" => "David",
        "edad" => 25
    ]
];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Arrays2</title>
</head>
<body>
<h1>Alumnos</h1>
<?php
    if (count($alumnos) > 0 ) {

    }
?>
<table>
    <tr>
        <?php
            foreach ($alumnos[0] as $clave => $dato)
                echo "<th>" . $clave . "</th>";
        ?>
    </tr>
    <?php
        foreach ($alumnos as $alumno) {
            echo "<tr>";

            foreach ($alumno as $dato)
                echo "<td>" . $dato . "</td>";
            echo "<tr>";
        }
    ?>
</table>
</body>
</html>