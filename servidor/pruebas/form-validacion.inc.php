<?php
function getClassRequiredField(
    string $nombreCampo,
    callable $fnCompruebaFormato = null) : string
{
    if ($_SERVER['REQUEST_METHOD'] !== 'POST')
        return '';

    if (empty($_POST[$nombreCampo]))
        return 'vacio';

    if (!is_null($fnCompruebaFormato)
        && $fnCompruebaFormato($nombreCampo) === false)
        return 'formatoIncorrecto';

    return '';
}

function filterEmail(string $nombreCampo) : bool
{
    $resultado = true;

    if (filter_input (
            INPUT_POST,
            $nombreCampo,
            FILTER_VALIDATE_EMAIL) === false)
        $resultado = false;

    return $resultado;
}

function cleanInput(string $nombreCampo) : string
{
    return htmlspecialchars(trim($_POST[$nombreCampo]));
}

function filterDate(string $nombreCampo) : bool
{
    $resultado = true;

    if (DateTime::createFromFormat(
            "d/m/Y",
            $_POST[$nombreCampo]) === false)
        $resultado = false;

    return $resultado;
}

function filterForm()
{
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (filterDate('fechaNacimiento') !== false)
            echo cleanInput('fechaNacimiento') . '<br>';
        if (filterEmail('email') !== false)
            echo cleanInput('email') . '<br>';
        echo cleanInput('observaciones');
    }
}
