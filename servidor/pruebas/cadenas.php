<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cadenas</title>
</head>
<body>
<h1>
    <?php
        $nombre = "Irene";

        // Elimina los espacios del principio y el final del nombre si los hubiera.
        echo "Hola " . trim($nombre);
        echo "<br>";

        // Elimina los caracteres „/‟ del principio y el final del nombre si los hubiera.
        echo "2. Hola " . trim($nombre ,"/");
        echo "<br>";

        /* La variable nombre en mayúsculas, minúsculas y con la primera letra en
        mayúscula y las demás en minúsculas.*/
        echo "Hola " . strtoupper($nombre);
        echo "<br>";
        echo "Hola " . strtolower($nombre);
        echo "<br>";
        echo "Hola " . ucwords($nombre);
        echo "<br>";

        // Muestra el código ascii de la primera letra del nombre.
        echo "ASCII primer caracter: " . ord($nombre);
        echo "<br>";

        //Muestra la longitud del nombre.
        echo "Longitud: " . strlen($nombre);
        echo "<br>";

        /*Muestra el número de veces que aparece la letra a (mayúscula o
        minúscula). */
        $nombre_mayus = strtoupper($nombre);
        echo "A aparece " . substr_count($nombre_mayus, "E");
        echo "<br>";

        /* Muestra la posición de la primera a existente en el nombre (sea mayúscula
        o minúscula). Si no hay ninguna mostrará -1.*/
        if (strpos($nombre_mayus, "E") === false) {
            echo "Posición de primera A: -1";
        }
        else {
            echo "Posición de A: " . strpos($nombre_mayus, "E");
        }
        echo "<br>";

        //Lo mismo, pero con la última a.
        if (strrpos($nombre_mayus, "E") === false) {
            echo "Posición de la última A: -1";
        }
        else {
            echo "Posición de la última A: " . strrpos($nombre_mayus, "E");
        }

        echo "<br>";

        /*Muestra el nombre sustituyendo las letras o por el número cero (sea
        mayúscula o minúscula). */
        echo str_ireplace("o", "0", $nombre);
        echo "<br>";

        // Indica si el nombre comienza por „al‟ o no.
        if (substr($nombre_mayus, 0, 2) === "AL") {
            echo "Comienza por al";
        }
        else {
            echo "No comienza por al";
        }

        echo "<br>";

        /* A partir de una variable que contenga una url, utiliza parse_url para
        extraer de la url las siguientes partes: */
        // El protocolo utilizado
        $url = "http://username:password@hostname:9090/path?arg=value#anchor";
        echo "Protocolo: " .parse_url($url, PHP_URL_SCHEME);
        echo "<br>";

        // El nombre de usuario
        echo "Usuario: " . parse_url($url, PHP_URL_USER);
        echo "<br>";

        // El path de la url
        echo "Path de url: " . parse_url($url, PHP_URL_PATH);
        echo "<br>";

        // El querystring de la url
        echo "Querystring: " . parse_url($url, PHP_URL_QUERY);
    ?>
</h1>
</body>
</html>