<?php
//insert into alumnos
//    (id, nombre, apellidos)
//values
//    (:id, :nombre, :apellidos)

function insert(string $tabla, array $campos)
{
    $claves = array_keys($campos);
    $nombresCampos = implode(', ', $claves);
    $nombresParametros = ':' . implode(', :', $claves);

    $sql = sprintf(
        "insert into %s (%s) values (%s)",
        $tabla, $nombresCampos, $nombresParametros);

    return $sql;
}

echo insert(
    'alumnos',
    [
        'id'=>1,
        'nombre'=>'Pedro',
        'apellidos'=>'G G'
    ]
);